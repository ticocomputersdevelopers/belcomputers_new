<?php
include "modules/classes/all.php";
$misc = new Misc($pgConn);

$sql = "SELECT roba_id, sifra_d, 'images/products/stare/'||sifra_d||'.jpg' AS putanja FROM roba WHERE sifra_d IS NOT NULL AND sifra_d <> '' ORDER BY sifra_d ASC";
$pgConn->query($sql,"slike",array());
$ret = array();
while($row = $pgConn->fetchArray()){
   $ret[]=$row;
};        

foreach ($ret as $slika){
	//Prvo vidi da li postoji fajl
	if(file_exists($slika['putanja'])){

		$web_slika_id = $misc->miscGetSequence('web_slika_web_slika_id_seq');
		$nova_putanja = 'images/products/big/'.$web_slika_id.'.jpg';
		//Prekopiramo fajl sa novim nazivom
		copy($slika['putanja'], $nova_putanja);

		//Insertujemo u bazu
		$sql = "INSERT INTO web_slika(web_slika_id, roba_id, akcija, putanja)VALUES (".$web_slika_id.", ".$slika['roba_id'].",1,'".$nova_putanja."') ";
		$pgConn->query($sql,"slike_insert".$slika['roba_id'],array());
	}	
}

?>