@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
<section id="main-content">
	<div class="row">
		<div class="small-12 medium-12 large-12 large-centered medium-centered columns flat-box" style="padding: 15px;">
			<h1 class="center ticket-title"> {{ Language::trans('Tiketi') }} </h1>
			<div class="text-right"><a href="{{TicketingOptions::base_url()}}ticketing">{{ Language::trans('Novi tiket') }}</a></div>
			<div class="table-responsive" style="overflow: auto;">
				<table class="user-orders-table">
					<tr>
						<th style="padding: 5px;">{{ Language::trans('Broj') }}.</th>
						<th style="padding: 5px;">{{ Language::trans('Naziv') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Opis') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Grupa') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Serijski broj') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Broj računa') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Proizvođač') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Hitno') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Napomena') }}</th>
						<th style="padding: 5px;">{{ Language::trans('Status') }}</th>
					</tr>
					<!-- thus repeat -->
					@foreach($radniNalozi->items as $row)
					<tr>	
						<td style="padding: 5px;">{{ $row->broj_naloga }}</td>
						<td style="padding: 5px;">{{ $row->uredjaj }}</td>
						<td style="padding: 5px;">{{ $row->opis_kvara }}</td>
						<td style="padding: 5px;">{{ $row->roba == 1 ? 'Roba' : 'Usluga' }}</td>
						<td style="padding: 5px;">{{ $row->serijski_broj }}</td>
						<td style="padding: 5px;">{{ $row->broj_fiskalnog_racuna }}</td>
						<td style="padding: 5px;">{{ $row->proizvodjac }}</td>
						<td style="padding: 5px;">{{ $row->hitno == 1 ? 'Da' : 'Ne' }}</td>
						<td style="padding: 5px;">{{ $row->napomena }}</td>
						<td style="padding: 5px;">{{ ($row->pregledan == 1) ? $row->status : 'Poslat zahtev' }}</td>
					</tr>
					@endforeach
					<!-- end repeat -->
				</table>
				{{ is_array($radniNalozi->items) ? Paginator::make($radniNalozi->items, $radniNalozi->count, $radniNalozi->limit)->links() : '' }}
			</div>
		</div>
	</div>
</section>
@endsection