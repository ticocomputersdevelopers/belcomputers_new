@extends('ticketing.templates.main')

@section('content')
<section id="main-content">
	<div class="row">
	     <div class="medium-6 medium-centered columns">
			<form class="flat-box front-page-servis" method="POST" action="{{RmaOptions::base_url()}}ticketing/promena-lozinke-post" style="padding: 15px;"> 
			<h1 class="ticket-title center">{{ 'Promena lozinke' }}</h1>
				<div class="field-group"> 
					<label for="">Stara lozinka</label>
					<div>
						<input name="old_password" class="{{ $errors->first('old_password') ? 'error' : '' }}" type="password" value="{{ Input::old('old_password') ? Input::old('old_password') : '' }}" autocomplete="off">
					</div>
				</div>
				<div class="field-group"> 
					<label for="">Nova lozinka</label>
					<div>
						<input name="password" class="{{ $errors->first('password') ? 'error' : '' }}" type="password" value="{{ Input::old('password') ? Input::old('password') : '' }}" autocomplete="off">
					</div>
				</div>
				<div class="field-group"> 
					<label for="">Potvrda nove lozinke</label>
					<div>
						<input name="password_confirmation" class="{{ $errors->first('password_confirmation') ? 'error' : '' }}" type="password" value="{{ Input::old('password_confirmation') ? Input::old('password_confirmation') : '' }}" autocomplete="off">
					</div>
				</div>
				<div class="btn-container"> 
					<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
				</div>
			</form>
		</div>
	</div>
</section>
@endsection