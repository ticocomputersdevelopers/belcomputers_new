
<?php $googleAnalytics = Options::googleAnalyticsID(); ?>
@if($googleAnalytics and $googleAnalytics->int_data == 1 and $googleAnalytics->str_data and $googleAnalytics->str_data != '')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{$googleAnalytics->str_data }}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{{$googleAnalytics->str_data }}');
</script>

@endif
