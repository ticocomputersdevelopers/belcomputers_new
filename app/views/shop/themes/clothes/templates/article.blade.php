<!DOCTYPE html>
<html lang="sr">
    <head>
        @include('shop/themes/'.Support::theme_path().'partials/head')
        <script type="application/ld+json">
        { 
            "@context" : "http://schema.org",
            "sku" : "{{ Product::sku($roba_id) }}",
            "mpn": "{{ Product::sku($roba_id) }}",
            "@type" : "Product",
            "name" : "{{ addslashes($title ) }}",
            "image" : "{{ Options::domain()."".Product::web_slika_big($roba_id) }}",
            "description" : "{{ addslashes(Product::seo_description($roba_id)) }}",
            "brand" : {
                "@type" : "Brand",
                "name" : "{{ Product::get_proizvodjac_name($roba_id) }}"
            },
            "offers" : {
                "@type" : "Offer",
                "price" : "{{ Product::get_price($roba_id) }}",
                "priceCurrency" : "{{ "RSD"; }}",
                "priceValidUntil" : "{{ date('Y-m-d', strtotime('+2 months')) }}",
                "availability" : "{{ (Product::ukupna_kolicina_proizvoda($roba_id) > 0 ? 'https://schema.org/InStock' : 'https://schema.org/OutOfStock') }}",
                "url" : "{{ Product::article_link($roba_id) }}"
            },
            "review": {
                "@type": "Review",
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": "4",
                    "bestRating": "5"
                },
                "author": {
                    "@type": "Organization",
                    "name": "{{ Options::company_name() }}"
                }
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "88",
                "bestRating": "100",
                "ratingCount": "20"
            }
        }
        </script> 

    </head>
    <body id="artical-page" 
    @if($strana == All::get_page_start()) id="start-page"  @endif 
    @if(Support::getBGimg() != null) 
        style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
    @endif
    >
        
            <!-- PREHEADER -->
            @include('shop/themes/'.Support::theme_path().'partials/menu_top')
            <!-- HEADER -->
            @include('shop/themes/'.Support::theme_path().'partials/header')

            @yield('article_details')

            
            <!-- FOOTER -->
            @include('shop/themes/'.Support::theme_path().'partials/footer')
       

        <!-- LOGIN POPUP -->
        @include('shop/themes/'.Support::theme_path().'partials/popups')

        <!-- BASE REFACTORING -->
        <input type="hidden" id="base_url" value="{{Options::base_url()}}" />
        <input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
        <input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />

        @if(Session::has('b2c_admin'.Options::server()))
        <script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
        <script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
        <script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
        <script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
        @endif

                <!-- js includes -->
        <script src="{{Options::domain()}}js/slick.min.js"></script>
        <!-- <script src="{{Options::domain()}}js/jquery.elevateZoom-3.0.8.min.js"></script> -->
        <!-- <script src="{{Options::domain()}}js/jquery.fancybox.pack.js"></script> -->

        <script src="{{Options::domain()}}js/shop/translator.js"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>
 
        @if(Options::header_type()==1)
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
        @endif
      </body>
</html>