@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="wrapper microsoft-page">
  <div>
    <div class="hero container">
      <div class="logos">
        <div class="row">
          <div class="col-6 text-left">
            <img src="images/microsoft-images/logo-contoso.png" width="139" alt="Contoso" />
          </div>
          <div class="col-6 text-right">
            <img src="images/microsoft-images/microsoft-logo.png" width="200" alt="Microsoft" />
          </div>
        </div>
      </div>
      <div class="row content">
        <div class="col-4 col-md-6 col-sm-12 text-left">
          <h1>Zamislite način na koji radite</h1>
          <img class="bar-divider" src="images/microsoft-images/img_bar-divider.jpg" width="238" alt="" />
          <p>Moderni uređaji sa operativnim sistemom Windows 11 i paketom programa Microsoft 365 ili Office 2021.</p>
          <a href="#" class="cta">Saznajte više  &gt;</a>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-12 text-center center-box outline-box">
        <h2>Hibridni rad je realnost.</h2>
        <p>Omogućavanje tehnologije ključno je za usvajanje agilnijih poslovnih modela, uključujući rad na daljinu. Uređaji stariji od 4 godine možda vas sputavaju. Budite u toku sa promenama uz upotrebu end-to-end rešenja za moderno radno mesto. Doživite kombinovanu snagu modernih uređaja sa Windows 11 i Microsoft 365 ili Office 2021, isplativog rešenja za produktivnost, kako biste svoje poslovanje poveli u budućnost. </p>
      </div>
    </div>
  </div>

  <div class="container side-by-side">
    <div class="row">
      <div class="col-6 col-md-12">
        <a href="###" target="_blank" class="play play-protection"></a>
      </div>
      <div class="col-6 col-md-12 text-left side-by-side-padd">
        <img class="side-by-side-icon" src="images/microsoft-images/icon-protect.png" width="100" alt="">
        <h2>Postignite moćnu zaštitu kojoj možete verovati sa novim, modernim uređajima koje pokreće Windows 11 </h2>
        <ul>
          <li>Zaštitite podatke i pristupite im s bilo kog mesta pomoću modela Zero Trust koji podržava operativni sistem Windows 11 </li>
          <li>Kupovinom računara dobijate snažnu ugrađenu sigurnost softvera i hardvera, šifriranje i zaštitu od zlonamernih programa</li>
          <li>Jednostavno primenite sigurnost bez lozinke poboljšanom tehnologijom Windows Hello za preduzeća<sup>3</sup></li>
          <li>Zero Trust kombinuje opcije sigurnosnosti pomoću provere autentičnosti na više nivoa u cilju zaštite korporativnog identiteta i podataka.</li>
          <li>Cloud config jednostavno štiti uređaj sa operativnim sistemom Windows 11 i priprema ga za pristup i rad sa odabranim aplikacijama </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="container side-by-side">
    <div class="row">
      <div class="col-6 col-md-12 order-md-2 text-left side-by-side-padd">
        <img class="side-by-side-icon" src="images/microsoft-images/icon-empower.png" width="100" alt="">
        <h2>Omogućite svom timu da bude produktivan i povezan sa bilo kog mesta</h2>
        <ul>
          <li>Jednostavan a moćan dizajn olakšaće vam rad i poboljšati produktivnost </li>
          <li>Brzo optimizujte prostor na ekranu</li>
          <li>Napravite prilagođene radne površine za svaki projekat</li>
          <li>Uključite ili isključite zvuk sa trake sa zadacima</li>
          <li>Radite prirodno i lako upotrebom olovke, glasa i dodira</li>
        </ul>
      </div>
      <div class="col-6 col-md-12 order-md-1">
        <a href="###" target="_blank" class="play play-empower"></a>
      </div>
    </div>
  </div>

  <div class="container side-by-side">
    <div class="row">
      <div class="col-6 col-md-12">
        <a href="###" target="_blank" class="play play-improve"></a>
      </div>
      <div class="col-6 col-md-12 text-left side-by-side-padd">
        <img class="side-by-side-icon" src="images/microsoft-images/icon-quickly.png" width="100" alt="">
        <h2>Brzo i jednostavno uključite radnike na daljinu pomoću operativnog sistema Windows 11</h2>
        <ul>
          <li>Učinite rad jednostavnijim pomoću kompatibilnosti aplikacije i upravljanja oblakom </li>
          <li>Jednostavna ažuriranja upravljačkih elemenata putem oblaka</li>
          <li> Jednostavno radite zajedno sa virtuelnim i lokalnim aplikacijama</li>
          <li>Pojednostavite upravljanje na daljinu pomoću aktivacije bez dodira</li>
        </ul>
      </div>
    </div>
  </div>

  <div class="full-graphic container empower">
    <div class="row">
      <div class="col-6 col-md-6 col-md-12 col-sm-12 text-left">
        <h2>Animirajte zaposlene pravim uređajem za njihov posao</h2>
        <br>
        <img class="bar-divider" src="images/microsoft-images/img_bar-divider.jpg" width="238" alt="" />
        <p>Najveći izbor uređaja na svetu i u različitim cenovnim razredima, od malih tableta do velikih višenamenskih uređaja, uključujući računare sa ekranom na dodir. Dobićete velike mogućnosti personalizacije, vrhunsko iskustvo i mogućnost upotrebe širokog spektra dodatne opreme.
        </p>
      </div>
    </div>
  </div>

  <div class="container reimagine-box">
    <div class="row">
      <div class="col-12 text-center center-box">
        <h2 style="max-width:700px;">Zamislite način na koji radite i uložite u savremenu produktivnost</h2>
        <p>Opremite svoje nove uređaje najnovijim alatkama za sigurnu saradnju, uključujući aplikacije za produktivnost, deljenje datoteka poslovne klase, ćaskanje, pozivanje i video konferencije. Sada je vreme da razmotrite prave alate za produktivnost koji najbolje odgovaraju vašem sledećem osvežavanju računara, bilo da je reč o jednokratnoj kupovini (Office Home & Business 2021) ili pretplati na softver Microsoft 365 za vaše poslovanje. </p>
      </div>
    </div>
  </div>

  <div class="container side-by-side">
    <div class="row">
      <div class="col-6 col-md-12 text-left side-by-side-padd office-icons-padd">
        <div class="value-stamp best-value">BEST VALUE</div>
        <h2 style="color:#0078d4;">Microsoft 365 Business Standard</h2>
        <div class="office-icons-top-border"></div>
        <h3>Premium Office Apps</h3>
        <div class="office-icons">
          <div class="office-icons-group">
            <img src="images/microsoft-images/office-premium-1.png" alt="" />
          </div>
          <div class="office-icons-group">
            <img src="images/microsoft-images/office-premium-2.png" alt="" />
          </div>
        </div>
        <h3>Additional Benefits</h3>
        <div class="office-icons">
          <div class="office-icons-group">
            <img src="images/microsoft-images/office-add.png" alt="" />
          </div>
          <div class="office-icons-group">
            <img src="images/microsoft-images/office-add-2.png" alt="" />
          </div>
        </div>
        <div class="office-icons-bottom-border"></div>
        <p class="font-size-14">Microsoft 365 Business Standard pomaže vam u vođenju i povećanju vašeg poslovanja zahvaljujući moćnim aplikacijama i uslugama za produktivnost. Vrhunska aplikacija Microsoft Teams koja dolazi uz Microsoft 365 Business Standard uključuje snimanje sastanaka, telefonske pozive, audio konferencije, skladištenje korisničkih datoteka od 1TB i još mnogo toga. Koristite jednu licencu za pokretanje instaliranih aplikacija Office na pet uređaja po korisniku; mobilnom uređaju, tabletima, računarima. Dostupno za Mac, iOS i Android uređaje uz stalnu telefonsku i mrežnu podršku Microsofta i pomoć u svakom trenutku. </p>
      </div>
      <div class="col-6 col-md-12 text-left side-by-side-padd office-icons-padd" style="background-color:#f6f6f6;">
        <div class="value-stamp">&nbsp;</div>
        <h2>Office Home &amp; Business 2021</h2>
        <div class="office-icons-top-border"></div>
        <h3>Classic Office Apps</h3>
        <div class="office-icons">
          <div class="office-icons-group">
            <img src="images/microsoft-images/office-classic-1.png" alt="" />
          </div>
          <div class="office-icons-group">
            <img src="images/microsoft-images/office-classic-2.png" alt="" />
          </div>
        </div>
        <hr>
        <p class="font-size-14 font-size-14-2">Vaš Office, modernizovan. Office Home & Business 2021 sa poznatim alatima za produktivnost pomaže vam da upravljate vašim poslovanjem. Uključuje Outlook, Word, Excel, PowerPoint i OneNote za operativni sistem Windows 11. Jednokratna kupovina omogućava instaliranje na jednom ličnom računaru ili Mac-u za kućnu ili poslovnu upotrebu. Office 2021 omogućava besplatno dodavanje aplikacije Microsoft Teams sa mogućnostima poput besplatnog ćaskanja, pretraživanja, zakazivanja sastanaka, čuvanje korisničkih datoteka od 2GB i pristupa gostima.
        </p>
      </div>
    </div>
  </div>

  <div class="container foot">
    <div class="row">
      <div class="col-12 text-center center-box" style="margin-top:0;">
        <p class="footnote">*Samo na ličnom računaru. Saznajte više (uključujući kompatibilne mobilne uređaje) na internet stranici office.com/information&nbsp; <br>
        <p class="footnote"> † Opcije OneNote razlikuju se u zavisnosti od platforme.https://aka.ms/onenote-office2019-faq&nbsp; </p>
        <p class="footnote"> ‡ Dostupnost aplikacije zavisi od uređaja/jezika. Idite na: office.com/systemrequirements za kompatibilne opcije operativnih sistema Window 11 i MacOS, kao za ostale dodatne opcije&nbsp; </p>
        <p class="footnote">** Aplikacija je dostupna i samostalno bez naplate</p>
        </p>

      </div>
    </div>
  </div>

  <div class="full-graphic container invest">
    <div class="row">
      <div class="col-6 col-md-6 col-md-12 col-sm-12 text-left">
        <h2>Uložite u modernu današnjicu</h2>
        <br>
        <img class="bar-divider" src="images/microsoft-images/img_bar-divider.jpg" width="238" alt="" />
        <a href="#" class="cta">Kupi sada &gt;</a>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-12 text-center center-box outline-box-white">
        <h2>Ostavljanje traga recikliranjem</h2>
        <p class="font-size-14">Microsoft je posvećen zaštiti sigurnosti i zdravlja zaposlenih, korisnika i javnosti putem politika i praksi za zaštitu okoline. Microsoft poštuje svoju predanost zaštiti životne sredine saradnjom sa industrijskim grupacijama i nevladinim organizacijama na uspostavljanju odgovornih praksi u izradi održivih proizvoda. U 2018. godini, Microsoft je reciklirao više od 1,7 miliona kilograma e-otpada i preusmerio ga sa deponija. Microsoft neprestano radi na poboljšanju radnog učinka i razgovara sa svojim dioničarima o postizanju ciljeva. Ako ste vi ili vaša kompanija / organizacija zainteresovani za pridonošenje inicijativama zaštite životne sredine kompanije Microsoft recikliranjem starog hardvera, posetite internet stranicu Proizvođača izvorne opreme za više informacija. Da biste pročitali više o posvećenosti kompanije Microsoft recikliranju, posetite našu internet stranicu: <br>
          <a href="https://www.microsoft.com/en-us/legal/compliance/recycling"
            target="_blank">https://www.microsoft.com/en-us/legal/compliance/recycling</a>.
          <br><br>
        </p>
      </div>
    </div>
  </div>
</div>

@endsection