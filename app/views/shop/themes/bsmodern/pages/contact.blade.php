@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CONTACT.blade -->

<div class="row"> 
	<div class="col-md-5 col-sm-12 col-xs-12 sm-no-padd">

		<br>

		<h2><span class="section-title">{{ Language::trans('Kontakt informacije') }}</span></h2>

		@if(Options::company_name() != '') 
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('Firma') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8"> {{ Options::company_name() }}</div>
		</div> 
		@endif

		@if(Options::company_adress() != '')
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('Adresa') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8"> {{ Options::company_adress() }}</div>
		</div>
		@endif
		
		@if(Options::company_city() != '')
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('Grad') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8"> {{ Options::company_city() }}</div>
		</div>
		@endif
		
		@if(Options::company_phone() != '')
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('Telefon') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8"> {{ Options::company_phone() }}</div>
		</div>
		@endif
		
		@if(Options::company_fax() != '')
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('Fax') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8"> {{ Options::company_fax() }}</div>
		</div>
		@endif
		
		@if(Options::company_pib() != '')
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('PIB') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8"> {{ Options::company_pib() }}</div>
		</div>
		@endif
		
		@if(Options::company_maticni() != '')
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('Matični broj') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8"> {{ Options::company_maticni() }}</div>
		</div>
		@endif
		
		@if( Options::company_email() != '')
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('E-mail') }}:</div>
			<div class="col-md-8 col-sm-8 col-xs-8">
				<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
			</div> 
		</div>
		@endif

		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4 no-padding">{{ Language::trans('Obrasci') }}:</div>
			<div class="col-md-7 col-sm-8 col-xs-8 text-bold">
				<a href="../files/pib.pdf" download>PIB</a>,
				<a href="../files/pdv.pdf" download>PDV</a>,
				<a href="../files/id.pdf" download>ID</a>
			</div> 
		</div>

		<br>

		<h2><span class="section-title">{{ Language::trans('Sektori') }}</span></h2>

		<div class="row sectors-contact">
			<div class="col-md-4 col-sm-4 col-xs-12 no-padding">
				<p class="text-bold"> {{ Language::trans('Komercijala') }}: </p>
				<ul>
	                <li><a href="mailto:branko.mihailovic@belcomputers.rs">Branko Mihailović</a></li>
	                <li><a href="mailto:srdjan.bulbuk@belcomputers.rs">Srđan Bulbuk</a></li>
	                <!-- <li><a href="mailto:dragan.vjetrovic@belcomputers.rs">Dragan VjetroviĂ¦</a></li> -->
	                <li><a href="mailto:tatjana.stosic@belcomputers.rs">Tatjana Stošić</a></li>
	                <li><a href="mailto:blaza.sofranic@belcomputers.rs">Blaža Sofranić</a></li>
	            </ul>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
            	<br class="visible-xs"> 
				<p class="text-bold"> {{ Language::trans('Magacin') }}: </p>
				<ul>
                    <li><a href="mailto:drago.lukic@belcomputers.rs">Drago Lukić</a></li>
                    <li><a href="mailto:dragan.ilic@belcomputers.rs">Dragan Ilić</a></li>
                </ul>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
         		<br class="visible-xs"> 
				<p class="text-bold"> {{ Language::trans('Servis') }}: </p>
				<ul>
                    <li><a href="mailto:aleksandar.micic@belcomputers.rs">Aleksandar Micić</a></li>
                    <!-- <li><a href="mailto:kosta.obradovic@belcomputers.rs">Kosta Obradovic</a></li> -->
                    <!-- <li><a href="mailto:aleksandar.vranic@belcomputers.rs">Aleksandar VraniĂ¦</a></li> -->
                    <li><a href="mailto:sasa.stankovic@belcomputers.rs">Saša Stanković</a></li>
                    <li><a href="mailto:milos.rajcevic@belcomputers.rs">Miloš Rajčević</a></li>
                    <li><a href="mailto:mihajlo.ilic@belcomputers.rs">Mihajlo Ilić</a></li>

                </ul>
            </div>
			 
		</div>

	</div> 


	<div class="col-md-7 col-sm-12 col-xs-12 sm-no-padd"> 

		<br>
		
		<h2><span class="section-title">{{ Language::trans('Pošaljite poruku') }}</span></h2>

		<form method="POST" action="{{ Options::base_url() }}contact-message-send">
			<div>
				<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
				<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
				<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
			</div> 

			<div>
				<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
				<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
				<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
			</div>		

			<div>	
				<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
				<textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
				<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
			</div> 

			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>

			<div class="text-right"> 
				<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
			</div>
		</form>
	</div>
</div>


@if(Options::company_map() != '' && Options::company_map() != ';')
<div class="map-frame relative">
	
	<div class="map-info">
		<h5>{{ Options::company_name() }}</h5>
		<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
	</div>

	<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

</div> 
@endif
 

@if(Session::get('message'))
<script>
	$(document).ready(function(){     
 
        bootboxDialog({ message: "<p>{{ Session::get('message') }}</p>" }); 

	});
</script>
@endif

<!-- CONTACT.blade END -->

@endsection     