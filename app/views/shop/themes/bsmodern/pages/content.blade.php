@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CONTENT.blade -->

@if($strana == All::get_page_start())
<div class="JSloader text-center hidden-sm hidden-xs">
	<div class="load-ring inline-block">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>
@endif  

@foreach($sekcije as $sekcija) 
<div {{ !empty($sekcija->boja_pozadine) ? 'style="background-color:'.($sekcija->boja_pozadine).'"' : (($sekcija->puna_sirina == 1) ? 'style="background-color: #fff"' : '') }}> 
	@if($sekcija->tip_naziv == 'Text')
	<!-- TEXT SECTION -->
	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12 padding-v-20">
				<div>
					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>
				</div>

				<div class="txt-wrapper">
					<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

						{{ Support::pageSectionContent($sekcija->sekcija_stranice_id) }}

					</div>
				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista artikala' AND !is_null($sekcija->tip_artikla_id))
	<!-- PRODUCTS SECTION -->
	<?php 
		$tipNaziv = DB::table('tip_artikla')->where('tip_artikla_id', $sekcija->tip_artikla_id)->pluck('naziv');
		$sluggedTipNaziv = Url_mod::slugify($tipNaziv); 
	?>

	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20 all-products-type">
			<div class="col-xs-12">
				<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

					@if($sekcija->tip_artikla_id == -1 AND count($latestAdded = Articles::latestAdded(5)) > 0)

						<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

						<div class="@if(All::action_page($strana)) JSlanding-p-product @else JSproducts_slick @endif"> 
							@foreach($latestAdded as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
							@endforeach
						</div> 

						@if(All::action_page($strana))
						<div class="text-center">
							<a href="{{ Options::base_url()}}tip/{{$sluggedTipNaziv}}" class="go-to-product-type inline-block"> {{Language::trans('Pogledaj sve')}} </a> 
						</div>
						@endif 

					@elseif($sekcija->tip_artikla_id == -2 AND count($mostPopularArticles = Articles::mostPopularArticles(5)) > 0)

						<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

						<div class="@if(All::action_page($strana)) JSlanding-p-product @else JSproducts_slick @endif"> 
							@foreach($mostPopularArticles as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
							@endforeach 
						</div> 

						@if(All::action_page($strana))
							<div class="text-center">
								<a href="{{ Options::base_url()}}tip/{{$sluggedTipNaziv}}" class="go-to-product-type inline-block"> {{Language::trans('Pogledaj sve')}} </a> 
							</div>
						@endif 

					@elseif($sekcija->tip_artikla_id == -3 AND count($bestSeller = Articles::bestSeller()) > 0)

						<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

						<div class="@if(All::action_page($strana)) JSlanding-p-product @else JSproducts_slick @endif"> 
							@foreach($bestSeller as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
							@endforeach
						</div> 

						@if(All::action_page($strana))
						<div class="text-center">
							<a href="{{ Options::base_url()}}tip/{{$sluggedTipNaziv}}" class="go-to-product-type inline-block"> {{Language::trans('Pogledaj sve')}} </a> 
						</div>
						@endif 

					@elseif($sekcija->tip_artikla_id == 0 AND All::broj_akcije() > 0)

						<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

						<div class="@if(All::action_page($strana)) JSlanding-p-product @else JSproducts_slick @endif"> 
							@foreach(Articles::akcija(null,20) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
							@endforeach
						</div> 

						@if(All::action_page($strana))
						<div class="text-center">
							<a href="{{ Options::base_url()}}tip/{{$sluggedTipNaziv}}" class="go-to-product-type inline-block"> {{Language::trans('Pogledaj sve')}} </a> 
						</div>
						@endif 

					@elseif(All::provera_tipa($sekcija->tip_artikla_id)) 

						<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2> 

						<div class="@if(All::action_page($strana)) JSlanding-p-product @else JSproducts_slick @endif"> 
							@foreach(Articles::artikli_tip($sekcija->tip_artikla_id,20) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
							@endforeach
						</div> 

						@if(All::action_page($strana))
							<div class="text-center">
								<a href="{{ Options::base_url()}}tip/{{$sluggedTipNaziv}}" class="go-to-product-type inline-block"> {{Language::trans('Pogledaj sve')}} </a> 
							</div>
						@endif   

					@endif

				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista vesti')
	<!-- BLOGS SECTION-->
	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">
				<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

					<div class="JSblog-slick row">   
						@foreach(All::getShortListNews() as $row) 
						<div class="col-md-4">
							<div class="news"> 
								@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))

								<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></a>

								@else

								<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

								@endif

								<h3 class="news-title overflow-hdn">
									<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
								</h3>

								<div class="text-uppercase">
									{{ Support::date_convert($row->datum) }}
								</div>
							</div>
						</div>
						@endforeach 
					</div> 

				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Slajder' AND !is_null($sekcija->slajder_id))

	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}">  

		@if($slajder = Slider::slajder($sekcija->slajder_id) AND count($slajderStavke = Slider::slajderStavke($slajder->slajder_id)) > 0)

		<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

			@if($slajder->slajder_tip_naziv == 'Slajder')

			<div class="JSmain-slider">
				@foreach($slajderStavke as $slajderStavka)
				<div class="relative">

					<a href="{{ $slajderStavka->link }}">
						<img class="img-responsive" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
					</a>

					<div class="sliderText"> 
						@if($slajderStavka->naslov != '') 
						<div class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov }}
						</div> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull hidden-sm hidden-xs" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="JSInlineShort inline-block btn-slider-txt" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov_dugme }}
						</a> 
						@endif
					</div>
				</div>
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Baner')
			<!-- BANNERS SECTION -->
			<div class="banners row padding-v-20">
				@foreach($slajderStavke as $slajderStavka)
				<div class="col-md-4 col-sm-4 col-xs-4">
					<a class="center-block" href="{{ $slajderStavka->link }}">
						<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
					</a>

					@if($slajderStavka->sadrzaj != '') 
					<div class="sliderText short-desc JSInlineFull content hidden-xs" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
						{{ $slajderStavka->sadrzaj }}
					</div> 
					@endif
				</div>  
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Baneri ikonice')
			<!-- BANNERS SECTION -->
			<div class="row padding-v-20 small-icons-section">
				@foreach($slajderStavke as $slajderStavka)
				<div class="col-md-2 col-sm-2 col-xs-4">
					<a class="center-block flex" href="{{ $slajderStavka->link }}">
						<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
					</a>

					@if($slajderStavka->sadrzaj != '') 
					<div class="short-desc text-center JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
						{{ $slajderStavka->sadrzaj }}
					</div> 
					@endif
				</div>  
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst levo)')
			<!-- TEXT BANNER LEFT SECTION -->
			@foreach($slajderStavke as $slajderStavka) 
			<div class="row padding-v-20 flex">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="txt-ban-text">   
						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="btn-slider-txt JSInlineShort inline-block" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov_dugme }}
						</a> 
						@endif
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12 txt_banner">
					<a href="{{ $slajderStavka->link }}" class="slider-link"></a>
					<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div>
				</div>
			</div>
			@endforeach

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst desno)')
			<!-- TEXT BANNER RIGHT SECTION -->
			@foreach($slajderStavke as $slajderStavka) 
			<div class="row padding-v-20 flex">
				<div class="col-md-6 col-sm-6 col-xs-12 txt_banner">
					<a href="{{ $slajderStavka->link }}" class="slider-link"></a>
					<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="txt-ban-text">   
						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="btn-slider-txt JSInlineShort inline-block" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov_dugme }}
						</a> 
						@endif
					</div>
				</div>
			</div>
			@endforeach

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner - Mali (tekst levo)')
			<!-- TEXT BANNER RIGHT SECTION -->
			@foreach($slajderStavke as $slajderStavka) 
			<div class="padding-v-20"> 
				<div class="border-bottom-section">
					<div class="row flex small-txt-ban">
						<div class="col-md-9 col-sm-9 col-xs-12 no-padding txt_banner">

							<div class="txt-ban-text">   
								@if($slajderStavka->naslov != '') 
								<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif

								@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
								@endif

								@if($slajderStavka->naslov_dugme != '') 
								<a href="{{ $slajderStavka->link }}" class="btn-slider-txt JSInlineShort inline-block" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->naslov_dugme }}
								</a> 
								@endif
							</div>
							
						</div>

						<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
							<a href="{{ $slajderStavka->link }}" class="slider-link"></a>
							<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div>
						</div>
					</div>
				</div>
			</div>
			@endforeach

			@elseif($slajder->slajder_tip_naziv == 'Galerija Baner')
			<!-- GALLERY BANNER SECTION -->
			<div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="gallery-ban">
						@if(isset($slajderStavke[0]))  
							<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavke[0]->image_path }}' )">
								<a href="{{ $slajderStavke[0]->link }}" class="slider-link"></a> 
									
								@if($slajderStavke[0]->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavke[0]->naslov }}
								</h2> 
								@endif
							</div>
						@endif

						@foreach(array_slice($slajderStavke,1) as $position => $slajderStavka) 
						
						@if($position == 0 || $position == 3)   
							<!-- <div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"> 
								<a href="{{ $slajderStavka->link }}" class="slider-link"></a> 
								
								@if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif
							</div>  -->
						@endif

						@if($position == 0 || $position == 1)   
							<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )">
								<a href="{{ $slajderStavka->link }}" class="slider-link"></a>
									
								@if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2>  
								@endif
							</div>
						@endif
						@endforeach 
					</div> 
				</div>
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Galerija Slajder')
			<!-- GALLERY SLIDER SECTION -->
			<div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="main_imgGallery flex">
	                    <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slajderStavke[0]->image_path }}" alt="image" />
	                </div>
	                <!-- CUSTOM MODAL -->
	                <div class="JSmodal">
	                    <div class="flex full-screen relative"> 
	                      
	                        <div class="modal-cont relative text-center"> 
	                      
	                            <div class="inline-block relative"> 
	                                <span class="JSclose-modal"><i class="fas fa-times"></i></span>
	                          
	                                <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
	                            </div>

	                            <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
	                            <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
	                        </div>
	                
	                    </div>
	                </div>
	     
					<div class="gallery_slider"> 
						@foreach($slajderStavke as $slajderStavka) 
							<a class="flex JSimg-gallery relative" href="javascript:void(0)">
									
								<img class="img-responsive" src="{{ Options::domain().$slajderStavka->image_path }}" id="{{ Options::domain().$slajderStavka->image_path }}" alt="image">

								<!-- @if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif -->

							</a> 
						@endforeach 
					</div>
				</div>
			</div>

			@endif  
		</div> <!-- JSd_content -->

		@endif
	</div> <!-- CONTAINER -->


	@elseif($sekcija->tip_naziv == 'Newsletter' AND Options::newsletter()==1 AND !is_null($newslatter_description = All::newslatter_description()))
	<!-- NEWSLETTER SECTION -->
	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}">  
		<div class="row {{ $strana == All::get_page_start() ? 'JSd_content' : '' }} padding-v-20 flex"> 

			<div class="col-md-6 col-sm-6 col-xs-12">
				<h5 class="ft-section-title JSInlineShort" data-target='{"action":"newslatter_label"}'>
					{{ $newslatter_description->naslov }}
				</h5> 
				<div class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
					{{ $newslatter_description->sadrzaj }}
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12"> 
				<div class="newsletter relative">		 
					<input type="text" placeholder="E-mail" id="newsletter" />

					<button onclick="newsletter()" class="button">{{ Language::trans('Prijavi se') }}</button>
				</div>
			</div>
		</div>
	</div> 

	@endif 
</div> <!-- END BACKGROUND COLOR -->
@endforeach 


@if(isset($anketa_id) AND $anketa_id>0)
@include('shop/themes/'.Support::theme_path().'partials/poll')		
@endif


@if(Session::has('pollTextError'))
<script>
	$(document).ready(function(){     
		bootboxDialog({ message: "<p>{{ Session::get('pollTextError') }}</p>" }); 
	});
</script>
@endif



<script>
	// LANDING PAGE TEXT SLIDER 
	if($(window).width() > 1440) {
		var container_left = $('.container').offset().left;
		if($('.landing-p').find('.JSmain-slider').length) {
			$('.landing-p').find('.sliderText').css('left', container_left);
		}
	}
	// HIDE TITLE - ICONS TEXT SECTION ON LANDING PAGE 
	if($('.icons-wrapper').length) { 
		$('.icons-wrapper').closest('.txt-wrapper').siblings('div').hide();
	}

	// HIDE FOOTER TITLE ON LANDING PAGE 
	$('.section-title').each(function(){
		if($(this).text().toLowerCase().includes('futer')) {
			$(this).hide();
		}
	});
</script>

<!-- CONTENT.blade END -->

@endsection
