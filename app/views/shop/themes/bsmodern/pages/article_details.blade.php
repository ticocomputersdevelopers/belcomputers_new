@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

<!-- ARTICLE_DETAILS.blade -->

<div id="fb-root"></div> 
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

@if(Session::has('success_add_to_cart'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Artikal je dodat u korpu') + ".</p>" }, 2200); 

    });
@endif

@if(Session::has('success_comment_message'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Vaš komentar je poslat') + ".</p>" }, 2200); 

    });
@endif
</script> 


<main class="d-content JSmain relative"> 

    <div class="container">
     
        <ul class="breadcrumb"> 
            {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
        </ul>
     
        <div class="row"> 
            
            <div class="JSproduct-preview-image col-md-6 col-sm-12 col-xs-12">
                <div class="row"> 
                    <div id="gallery_01" class="col-md-3 col-sm-3 col-xs-12 text-center sm-no-padd">

                        @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)                             
                            @if(Product::pitchPrintImageExist($roba_id) == TRUE)                          
                                <a class="elevatezoom-gallery JSp_print_small_img" 
                                href="javascript:void(0)" 
                                data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                                data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766">
                               
                                <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                                alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766"/> 

                                </a>
                            @endif   
                                <a class="elevatezoom-gallery JSp_print_small_img" 
                                href="javascript:void(0)" 
                                data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                                data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                               
                                <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                                alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766"/> 

                            </a>   

                        @else

                            @foreach($slike as $image)
                            <a class="elevatezoom-gallery" href="javascript:void(0)" data-image="/{{$image->putanja}}" data-zoom-image="{{ Options::domain().$image->putanja }}">

                                <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>

                            </a>
                            @endforeach
                        @endif
                    </div>

                    <div class="col-md-9 col-sm-6 col-xs-12 disableZoomer relative"> 

                        <a class="fancybox" href="{{ Options::domain() }}{{ $slika_big }}">

                            @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)

                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                                    
                                    <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" id="art-img" class="JSzoom_03 img-responsive" alt="{{ Product::seo_title($roba_id)}}" />
                                
                                </span>

                            @else
                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="{{ Options::domain() }}{{ $slika_big }}">
                                   
                                    <img class="JSzoom_03 img-responsive" id="art-img" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" />

                                </span>
                            @endif
                        </a>

                        <div class="product-sticker flex">
                            @if( B2bArticle::stiker_levo($roba_id) != null )
                                <a class="article-sticker-img">
                                    <img class="img-responsive" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}"  />
                                </a>
                            @endif 
                            
                            @if( B2bArticle::stiker_desno($roba_id) != null )
                                <a class="article-sticker-img clearfix">
                                    <img class="img-responsive pull-right" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}"  />
                                </a>
                            @endif   
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 additional_img">    
                        @foreach($glavne_slike as $slika)
                        <a class="inline-block text-center" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}/{{$slika->web_slika_id}}"> 
                            <img src="{{ Options::domain().$slika->putanja }}" alt=" {{ Options::domain().$slika->putanja }}" class="img-responsive inline-block"> 
                        </a>
                        @endforeach
                    </div>
                </div>
               
            </div>

            <div class="product-preview-info col-md-6 col-sm-12 col-xs-12">

                <div class="row flex"> 
                    <div class="@if($proizvodjac_id != -1) col-md-10 col-sm-10 col-xs-9  @else col-xs-12 @endif no-padding">
                        <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>
                    </div>

                    @if($proizvodjac_id != -1)
                    <div class="col-md-2 col-sm-2 col-xs-3 md-no-padd-right text-right">
                        @if( Product::slikabrenda($roba_id) != null )
                       
                        <a class="article-brand-img inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                            <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                        </a>

                        @else

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}" class="artical-brand-text inline-block">{{ product::get_proizvodjac($roba_id) }}</a>

                        @endif                                   
                    </div>
                    @endif     
                </div>

                <div class="review">{{ Product::getRating($roba_id) }}</div>

                <!-- ADDITIONAL INFO -->
                <div class="flex additional_info_article">
                    <!-- ARTICLE PASSWORD -->
                    <ul>
                        @if(AdminOptions::sifra_view_web()==1)
                        <li class="info-article"> <span> {{Language::trans('Šifra') }}:</span> <span> {{Product::sifra($roba_id)}} </span> </li>
                        @elseif(AdminOptions::sifra_view_web()==4)                       
                        <li class="info-article"> <span> {{Language::trans('Šifra') }}:</span> <span> {{Product::sifra_d($roba_id)}} </span> </li>
                        @elseif(AdminOptions::sifra_view_web()==3)                       
                        <li class="info-article"> <span> {{Language::trans('Šifra') }}:</span> <span> {{Product::sku($roba_id)}} </span> </li>
                        @elseif(AdminOptions::sifra_view_web()==2)                       
                        <li class="info-article"> <span> {{Language::trans('Šifra') }}:</span> <span> {{Product::sifra_is($roba_id)}} </span> </li>
                        @endif
                    </ul>

                    @if($grupa_pr_id != -1)
                    <ul>
                        <li class="info-article"> <span> {{Language::trans('Proizvod iz grupe')}}:</span> <span> {{ Product::get_grupa($roba_id) }} </span> </li>
                    </ul>
                    @endif 

                    @if($proizvodjac_id != -1) 
                        <ul>
                            <li class="info-article">
                                <span>{{Language::trans('Proizvođač')}}:</span>

                                @if(Support::checkBrand($roba_id))
                                <span>
                                    <a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">{{Product::get_proizvodjac($roba_id)}}</a>
                                </span>
                                @else
                                <span>{{Product::get_proizvodjac($roba_id)}}</span>
                                @endif
                            </li>
                        </ul>
                    @endif 

                    @if(Options::vodjenje_lagera() == 1)
                    <ul>
                          <li class="info-article"> <span> {{Language::trans('Dostupna količina')}}:</span><span>{{Cart::check_avaliable($roba_id)}}</span></li>  
                    </ul>
                    @endif  
                    
                    @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                    <ul>
                        <li class="info-article"><span>{{Language::trans('Težina artikla')}}:</span><span>{{Product::tezina_proizvoda($roba_id)/1000}} kg</span></li>
                    </ul>
                    @endif 
                </div>
                <!-- END ADDITIONAL INFO -->

                <a class="delivery-info img-responsive" href="/nacin-kupovine"> <img src="{{ Options::domain() }}images/delivery-info.jpg"> </a>
                <!-- PRICE -->
                <div class="product-preview-price">
                    @if(Product::getStatusArticlePrice($roba_id) == 1)

    <!--                         @if(Product::pakovanje($roba_id)) 
                        <div> 
                            <span class="price-label">{{ Language::trans('Pakovanje') }}:</span>
                            <span class="price-num">{{ Product::ambalaza($roba_id) }}</span> 
                        </div>
                        @endif  -->                                

                        @if(All::provera_akcija($roba_id))    

                        @if(Product::get_mpcena($roba_id) != 0) 
                        <div>
                            <span class="price-label">{{ Language::trans('Maloprodajna cena') }}:</span>
                            <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                        </div>
                        @endif  

                        <div> 
                            <span class="price-label">{{ Language::trans('Akcijska cena')}}:</span> 
                            <span class="JSaction_price price-num main-price" data-action_price="{{Product::get_price($roba_id)}}">
                                {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                            </span>
                        </div>

                        @if(Product::getPopust_akc($roba_id)>0)
                        <div> 
                            <span class="price-label">{{ Language::trans('Popust') }}: </span>
                            <span class="price-num discount">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</span>
                        </div>
                        @endif

                        @else

                        @if(Product::get_mpcena($roba_id) != 0) 
                        <div> 
                            <span class="price-label">{{ Language::trans('Maloprodajna cena') }}:</span>
                            <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                        </div>
                        @endif

                        <div> 
                            <span class="price-label">{{ Language::trans('WebCena') }}:</span>
                            <span class="JSweb_price price-num main-price" data-cena="{{Product::get_price($roba_id)}}">
                               {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                           </span>
                       </div>

                       @if(Product::getPopust($roba_id)>0)
                           @if(AdminOptions::web_options(132)==1)
                           <div> 
                            <span class="price-label">{{ Language::trans('Popust') }}:</span>
                            <span class="price-num discount">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                            </div>
                            @endif
                        @endif

                    @endif
                @endif 
            </div>



            <div class="add-to-cart-area clearfix">    

             @if(Product::getStatusArticle($roba_id) == 1)
                 @if(Cart::check_avaliable($roba_id) > 0)
                 <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm"> 
                  
                    <!-- PITCHPRINT -->
                    @if(!empty(Product::design_id($roba_id) AND !empty(Options::pitchprint() AND Options::pitchprint_aktiv() == 1)))
                        @include('shop/themes/'.Support::theme_path().'partials/pitchprint')
                    @endif
                    <!-- PITCHPRINT END -->

                @if(Product::check_osobine($roba_id))  
                    @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)

                    <div class="attributes text-bold">

                        <div>{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>

                        @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                        <label class="relative no-margin" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">

                            <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>

                            <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                            
                        </label>
                        @endforeach

                    </div>

                    @endforeach 
                @endif

                @if(AdminOptions::web_options(313)==1) 
                <div class="num-rates"> 
                    <div> 
                        <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                    </div>
                    <select class="JSinterest" name="kamata">
                        {{ Product::broj_rata(Input::old('kamata')) }}
                    </select>
                </div>
                @endif

                <div class="printer inline-block" title="{{ Language::trans('Štampaj') }}">  
                    <a href="{{Options::base_url()}}stampanje/{{ $roba_id }}" target="_blank" rel="nofollow"><i class="fas fa-print"></i></a>
                </div>

                @if(Cart::kupac_id() > 0)
                <button class="like-it JSadd-to-wish" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button>
                @else
                <button class="like-it JSnot_logged" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                @endif


                <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                <span>&nbsp;{{Language::trans('Količina')}}&nbsp;</span>
                <div class="inline-flex">
                    @if(Options::web_options(320) == 1 AND (Product::jedinica_mere($roba_id)->jedinica_mere_id) == 3 AND Product::pakovanje($roba_id) ) 
                        <input type="number" name="kolicina" class="cart-amount weight" min="0" step="0.1" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}"><span>&nbsp;{{Language::trans(' kg')}}&nbsp;</span>
                    @else
                        <input type="text" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">
                        
                    @endif
                    <button type="submit" id="JSAddCartSubmit" class="add-to-cart button">{{Language::trans('U korpu')}}</button>
                    <input type="hidden" name="projectId" value=""> 
                </div>
                <div class="red-dot-error">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  

            </form>

            @else

            <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>       
            @endif

            @else
            <button class="button" data-roba-id="{{$roba_id}}">
                {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
            </button>
            @endif
        </div>


        @if(!empty(Product::get_labela($roba_id)))
        <div class="custom-label inline-block relative">
            <i class="fa fa-info-circle"></i>
            {{Product::get_labela($roba_id)}} 
        </div>
        @endif  

        <div class="information-article-text"> ** {{Language::trans('Bel Computers ne garantuje za ispravnost prikazanih slika i specifikacija, molimo Vas da iste proverite i na sajtu proizvođača pre poručivanja')}}. </div>

        <div class="facebook-btn-share flex">

            <div class="soc-network inline-block"> 
                <div class="fb-like" data-href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </div>

            <div class="soc-network"> 
                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </div>  
        </div>

        <!-- ADMIN BUTTON-->
        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
        <div class="admin-article"> 
            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
            <a class="JSFAProductModalCall article-level-edit-btn" data-roba_id="{{$roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
            @endif
            <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
            <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
        </div>
        @endif
    </div>
    </div>

    @if(count($srodni_artikli) > 0)
        <div class="related-custom JSproducts_slick row"> 
        @foreach($srodni_artikli as $srodni_artikl)
        <div class="JSproduct col-md-4 col-sm-4 col-xs-12">  
            <div class="card flex row">  
                <div class="col-xs-3 no-padding">
                    <div class="img-wrap"> 
                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                            <img class="img-responsive" src="{{ Options::domain() }}{{ Product::web_slika_big($srodni_artikl->srodni_roba_id) }}" alt="{{ Product::seo_title($srodni_artikl->srodni_roba_id)}}" />
                        </a>
                    </div>
                </div>

                <div class="col-xs-9">
                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                        <h2 class="title">{{ Product::seo_title($srodni_artikl->srodni_roba_id) }}</h2>
                    </a>

                    <div>{{ Product::get_karakteristika_srodni($srodni_artikl->grupa_pr_vrednost_id) }}</div>

                    <div class="price"> 
                        <span>{{ Cart::cena(Product::get_price($srodni_artikl->srodni_roba_id)) }}</span>
                    </div>  
                </div>   
            </div>
        </div>
        @endforeach 
        </div>
    @endif

    @if(Options::checkTags() == 1)
        @if(Product::tags($roba_id) != '')
        <div class="product-tags">  
            <div>{{ Language::trans('Tagovi') }}:</div>

            <h6 class="text-white inline-block">
                {{ Product::tags($roba_id) }} 
            </h6>
        </div>       
        @endif
    @endif    

    <!-- PRODUCT PREVIEW TABS-->
    <div id="product_preview_tabs" class="product-preview-tabs row">
    <div class="col-xs-12"> 
        <ul class="nav nav-tabs tab-titles">
            <li class="{{ !Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#description-tab" rel="nofollow">{{Language::trans('Specifikacija')}}</a></li>
            <li><a data-toggle="tab" href="#service_desc-tab" rel="nofollow">{{Language::trans('Opis')}}</a></li>
            <li><a data-toggle="tab" href="#technical-docs" rel="nofollow">{{Language::trans('Sadržaji')}}</a></li>
            <li class="{{ Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#the-comments" rel="nofollow">{{Language::trans('Komentari')}}</a></li>
        </ul>

        <div class="tab-content"> 

            <div id="description-tab" class="tab-pane fade{{ !Session::has('contactError') ? ' in active' : '' }}">
                {{ Product::get_karakteristike($roba_id) }}
            </div>


            <div id="service_desc-tab" class="tab-pane fade">
                {{ Product::get_opis($roba_id) }} 
                
                @if(Product::get_proizvodjac_name($roba_id)=='Bosch')
                <script type="text/javascript"></script>

                <div class="loadbeeTabContent" data-loadbee-apikey="{{Options::loadbee()}}" data-loadbee-gtin="{{Product::get_barkod($roba_id)}}" data-loadbee-locale="sr_RS"></div>

                <script src="https://cdn.loadbee.com/js/loadbee_integration.js" async=""></script>
                @endif
                <div id="flix-minisite"></div>
                <div id="flix-inpage"></div>
                <!-- fixmedia -->
                <script type="text/javascript" src="//media.flixfacts.com/js/loader.js" data-flix-distributor="{{Options::flixmedia()}}" data-flix-language="rs" data-flix-brand="{{Product::get_proizvodjac_name($roba_id)}}" data-flix-mpn="" data-flix-ean="{{Product::get_barkod($roba_id)}}" data-flix-sku="" data-flix-button="flix-minisite" data-flix-inpage="flix-inpage" data-flix-button-image="" data-flix-price="" data-flix-fallback-language="en" async>                                    
                </script>
            </div>


            <div id="technical-docs" class="tab-pane fade">
                @if(Options::web_options(120))
                    @if(count($fajlovi) > 0) 

                        @foreach($fajlovi as $row)
                        <div class="files-list-item">
                            <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                <div class="files-list-item">
                                    <div class="files-name">{{ Language::trans($row->naziv) }}</div> <!-- {{ Product::getExtension($row->vrsta_fajla_id) }} --> 
                                </div>
                            </a>
                        </div>
                        @endforeach 
                    @endif
                @endif
            </div>


            <div id="the-comments" class="tab-pane fade{{ Session::has('contactError') ? ' in active' : '' }}">
                <div class="row"> 
                    <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                    if($query_komentary->count() > 0){?>
                        <div class="col-md-6 col-sm-12 col-xs-12"> 
                            <ul class="comments">
                                <?php foreach($query_komentary->orderBy('web_b2c_komentar_id', 'DESC')->get() as $row)
                                { ?>
                                    <li class="comment">
                                        <ul class="comment-content relative">
                                            <li class="comment-name">{{$row->ime_osobe}}</li>
                                            <li class="comment-date">{{$row->datum}}</li>
                                            <li class="comment-rating">{{Product::getRatingStars($row->ocena)}}</li>
                                            <li class="comment-text">{{ $row->pitanje }}</li>
                                        </ul>
                                        <!-- REPLIES -->
                                        @if($row->odgovoreno == 1)
                                        <ul class="replies">
                                            <li class="comment">
                                                <ul class="comment-content relative">
                                                    <li class="comment-name">{{ Options::company_name() }}</li>
                                                    <li class="comment-text">{{ $row->odgovor }}</li>
                                                </ul>
                                            </li>
                                        </ul>
                                        @endif
                                    </li>
                                <?php }?>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="col-md-6 col-sm-12 col-xs-12"> 
                        <form method="POST" action="{{ Options::base_url() }}comment-add">
                            <label>{{Language::trans('Vaše ime')}}</label>
                            <input name="comment-name" type="text" value="{{ Input::old('comment-name') }}" {{ $errors->first('comment-name') ? 'style="border: 1px solid red;"' : '' }} />
                    
                            <label for="JScomment_message">{{Language::trans('Komentar')}}</label>
                            <textarea class="comment-message" name="comment-message" rows="5"  {{ $errors->first('comment-message') ? 'style="border: 1px solid red;"' : '' }}>{{ Input::old('comment-message') }}</textarea>
                            <input type="hidden" value="{{ $roba_id }}" name="comment-roba_id" />
                            <span class="review JSrev-star">
                                <span>{{Language::trans('Ocena')}}:</span>
                                <i id="JSstar1" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar2" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar3" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar4" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar5" class="far fa-star review-star" aria-hidden="true"></i>
                                <input name="comment-review" id="JSreview-number" value="0" type="hidden"/>
                            </span>
                            <div class="capcha text-center"> 
                                {{ Captcha::img(5, 160, 50) }}<br>
                                <span>{{ Language::trans('Unesite kod sa slike') }}</span>
                                <input type="text" name="captcha-string" tabindex="10" autocomplete="off" {{ $errors->first('captcha') ? 'style="border: 1px solid red;"' : '' }}>
                            </div>
                            <button class="pull-right button">{{Language::trans('Pošalji')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- BRENDOVI SLAJDER -->
                <!--  <div class="row">
                     <div class="col-md-12">
                        <div class="dragg JSBrandSlider"> 
                               <?php //foreach(DB::table('proizvodjac')->where('brend_prikazi',1)->get() as $row){ ?>
                            <div class="col-md-12 col-sm-6 end sub_cats_item_brend">
                                <a class="brand-link" href="{{Options::base_url()}}{{ Url_mod::slug_trans('proizvodjac') }}/<?php //echo $row->naziv; ?> ">
                                     <img src="{{ Options::domain() }}<?php //echo $row->slika; ?>" />
                                 </a>
                            </div>
                            <?php //} ?>
                        </div>
                     </div>
                 </div> -->

                 @if(Options::web_options(118))
                 <br>
                     @if(count($vezani_artikli))
                     <h2><span class="section-title">{{Language::trans('Vezani artikli')}}</span></h2>
                     
                     <div class="JSproducts_slick row">

                        @foreach($vezani_artikli as $vezani_artikl)
                        @if(Product::checkView($vezani_artikl->roba_id))
                        <div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="shop-product-card relative"> 
                                <!-- PRODUCT IMAGE -->
                                <div class="product-image-wrapper flex relative">

                                    <a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">
                                        <img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($vezani_artikl->vezani_roba_id) }}" alt="{{ Product::seo_title($vezani_artikl->vezani_roba_id) }}" />
                                    </a>

                                    <!-- <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}" class="article-details hidden-xs">
                                        <i class="fas fa-search-plus"></i> {{Language::trans('Detaljnije')}}</a> -->
                                    </div>

                                    <div class="product-meta">
                                        <div> 
                                            <span class="review">
                                                {{ Product::getRating($vezani_artikl->roba_id) }}
                                            </span>
                                        </div>
                                        <!-- PRODUCT PRICE -->
                                        <div class="price-holder">
                                         {{ Cart::cena(Product::get_price_vezani($vezani_artikl->roba_id,$vezani_artikl->vezani_roba_id)) }}
                                     </div>   

                                     <h2 class="product-name">
                                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">  {{ Product::short_title($vezani_artikl->vezani_roba_id) }}</a>
                                    </h2>

                                    @if($vezani_artikl->flag_cena == 1)
                                    <div class="add-to-cart-container flex justify-center">
                                        <!-- WISH LIST  --> 
                                        @if(Cart::kupac_id() > 0)
                                        <button class="like-it JSadd-to-wish" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{Language::trans('Dodaj na listu želja')}}"><i class="far fa-heart"></i></button>
                                        @else
                                        <button class="like-it JSnot_logged" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                                        @endif   

                                        @if(Product::getStatusArticle($vezani_artikl->roba_id) == 1)
                                            @if(Cart::check_avaliable($vezani_artikl->vezani_roba_id) > 0)
                                                <button class="buy-btn button JSadd-to-cart-similar" data-vezani_roba_id="{{ $vezani_artikl->vezani_roba_id }}" data-roba_id="{{ $vezani_artikl->roba_id }}">
                                                {{Language::trans('U korpu')}}</button>
                                            <!-- <input type="text" class="JSkolicina linked-articles-input like-it" value="1" onkeypress="validate(event)"> -->
                                           
                                            @else  
                                           
                                            <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>
                                           
                                            @endif
                                           
                                            @else
                                            <button class="buy-btn button">
                                                {{ Product::find_flag_cene(Product::getStatusArticle($vezani_artikl->roba_id),'naziv') }}
                                            </button>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                <!-- ADMIN BUTTON -->
                                @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" href="#">{{Language::trans('IZMENI ARTIKAL')}}</a>
                                @endif
                            </div>
                        </div>
                        @endif
                        @endforeach 
                    </div> 
                    @endif
                @endif

                @if(count(Product::get_related($roba_id)))
                    <!-- RELATED PRODUCTS --> 
                    <br>
                    <h2><span class="section-title">{{Language::trans('Srodni proizvodi')}}</span></h2>
                    <div class="JSproducts_slick row">
                        @foreach(Product::get_related($roba_id) as $row)
                            @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                        @endforeach
                    </div>
                @endif

            </div>
        </div> 
    </div> 
</main>

<!-- ARTICLE_DETAILS.blade -->

@endsection