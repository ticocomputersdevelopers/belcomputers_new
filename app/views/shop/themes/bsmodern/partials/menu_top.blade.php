<!-- MENU_TOP.blade -->
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal" rel="nofollow"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave" rel="nofollow"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin" rel="nofollow"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

@if(!All::action_page($strana))
<div class="preheader">

    <div class="social-icons hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container"> 
        <div class="row top-menu flex relative">
            <div class="col-md-6 col-sm-6 col-xs-8">   
                <div class="mailbox padding-v-15">
                    <span class="hidden-sm hidden-xs"> {{ Language::trans('Ukoliko želite dodatne informacije kontaktirajte nas na e-maila') }}:</span>
                    
                    @if(Options::company_email())
                    <a class="mailto" href="mailto:{{ Options::company_email() }}">
                        <i class="fas fa-envelope hidden-lg hidden-md"></i> {{ Options::company_email() }}
                    </a>
                    @endif
                </div> 
            </div>

            <div class="col-md-6 col-sm-6 col-xs-4 row text-right sm-static">

                @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block text-white hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif 

                <ul class="hidden-small JStoggle-content">
                    @foreach(All::menu_top_pages() as $row)
                    <li><a class="center-block" href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul> 

                 <!-- OPEN LOGIN AND REGISTRATION ON MOBILE -->
                <div class="JSopen-user inline-block"> 
                    <i class="fa fa-user hidden-lg hidden-md hidden-sm text-white relative"></i>
                    <div class="JSmobile-user">
                        @if(Session::has('b2c_kupac'))

                        <div class="logged-dropdown-user">
                            @if(WebKupac::get_user_name())
                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}" rel="nofollow" class="inline-block">{{ WebKupac::get_user_name() }}</a>
                            @endif

                            @if(WebKupac::get_company_name())
                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}" rel="nofollow" class="inline-block">{{ WebKupac::get_company_name() }}</a>
                            @endif

                            <div class="inline-block"> 
                                <span class="JSbroj_wish fas fa-heart"> {{ Cart::broj_wish() }} </span>  

                                <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a>
                            </div>
                        </div>
                        @else 
                        <ul class="login-dropdown">
                            <!-- ====== LOGIN MODAL TRIGGER ========== -->
                            <li>
                                <a href="#" data-toggle="modal" data-target="#loginModal" rel="nofollow">
                                {{ Language::trans('Prijava') }}</a>
                            </li>
                            <li>
                                <a href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}" rel="nofollow"> 
                                {{ Language::trans('Registracija') }}</a>
                            </li>
                        </ul>
                        @endif
                    </div> 
                </div>
            </div>   
 
        </div> 
    </div>
</div>
@endif
<!-- MENU_TOP.blade END



