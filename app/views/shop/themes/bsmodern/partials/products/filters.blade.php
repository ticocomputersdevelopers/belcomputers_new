<?php // echo $kara; ?>
<div class="filters"> 
	<div>  
		<div class="clearfix JShidden-if-no-filters">
			<span>{{ Language::trans('Izabrani filteri') }}:</span>
			<a class="JSreset-filters-button inline-block pull-right" role="button" href="javascript:void(0)" rel=”nofollow”>{{ Language::trans('Poništi filtere') }} <span class="fas fa-times"></span></a>
		</div> 

		<ul class="selected-filters">
			<?php $br=0;
			foreach($niz_proiz as $row){
				$br+=1;
				if($row>0){
					?>
					<li>
						{{All::get_manofacture_name($row)}}     
						<a href="javascript:void(0)" rel=”nofollow”>
							<span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
						</a>
					</li>
			<?php }}

			$br=0;
			foreach(All::getGroupedCharac($niz_karakteristike) as $key => $val){
				$br+=1;
				if($val != 0){
			?>
				<li>
					{{Language::trans($key)}}
					<a href="javascript:void(0)" rel=”nofollow”>
				        <span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$val}}"></span>
				    </a>
				</li>
				<?php }}
			if($strana == 'pretraga'){
			foreach($niz_grupa as $row){
			$br+=1;
			if($row>0){
				?>
				<li>
					{{Groups::getGrupa($row)}}
					<a href="javascript:void(0)" rel=”nofollow”>
						<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
					</a>
				</li>
			<?php }}}?>
		</ul>
			<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
			<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>	
			@if($strana == 'pretraga')	
			<input type="hidden" id="JSGroups_ids" value="{{$grupa_ids}}"/>
			@endif

	</div>	 
 		<ul>   
			@if(count($manufacturers)>0)
			<li>
				<a href="javascript:void(0)" class="filter-links center-block JSfilters-slide-toggle" rel=”nofollow”>{{ Language::trans('Proizvođač') }}
					<i class="fas fa-angle-up pull-right"></i>
				</a>

				<div class="JSfilters-slide-toggle-content">
					@foreach($manufacturers as $key => $value)
					@if(in_array($key,$niz_proiz))
						<label class="flex">
							<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
							<span class="filter-text center-block">
								{{All::get_manofacture_name($key)}} 
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
					@else
						<label class="flex">
							<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
							<span class="filter-text center-block">
								{{All::get_manofacture_name($key)}} 
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
					@endif
					@endforeach
				</div>				 				 
			</li>
			@endif				
			@if($characteristics > 0)
				@foreach($characteristics as $keys => $values)				
				<li>
					<a href="javascript:void(0)" class="filter-links center-block JSfilters-slide-toggle" rel=”nofollow”>
						{{Language::trans($keys)}} 
						<i class="fas fa-angle-up pull-right"></i>  							
					</a>

					<div class="JSfilters-slide-toggle-content">
						@foreach($values as $key => $value)
						<?php if(!array_diff(explode('-',$value['ids']),$niz_karakteristike)){ ?>
						<label class="flex">
							<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $value['ids'] }}" checked>
							<span class="filter-text center-block">
								{{ Language::trans($key) }}
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value['count'] }} @endif
							</span>
						</label>
						<?php }else {?>
						<label class="flex">
							<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $value['ids'] }}"> 
							<span class="filter-text center-block">
								{{ Language::trans($key) }}
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value['count'] }} @endif
							</span>
						</label>
						<?php } ?>
						@endforeach
					</div>
				</li>
				@endforeach	
			@endif		
		</ul>	 

		<input type="hidden" id="JSfilters-url" value="{{$url}}" />

		

		<!-- SLAJDER ZA CENU -->
		<div class="text-center"> 
			<br>
			<span class="">{{Articles::get_valuta()}}</span>
			<span id="JSamount" class="filter-price"></span>
			<div id="JSslider-range"></div><br>  
		</div>
		<!-- <span type="hidden" id="JSExcange" value= "{{Articles::get_valuta()}}"> </span> -->

	@if($strana == 'pretraga')
		<div class="manufacturer-categories">  
			<ul>
				@foreach($grupe as $key => $value)
				<li>
					<a href="#" data-key="{{ $key }}" class="JSGrupa_id">{{ Language::trans(Groups::getGrupa($key) ) }}</a>
				</li>
				@endforeach
			</ul>
		</div>	
	@endif

		<script>
			var max_web_cena_init = {{ $max_web_cena }};
			var min_web_cena_init = {{ $min_web_cena }};
			var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
			var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
			var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
		</script> 			 
			 
</div>


