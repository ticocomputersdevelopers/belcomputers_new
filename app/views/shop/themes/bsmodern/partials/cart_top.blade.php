<div class="col-md-2 col-sm-1 col-xs-2 text-center no-padding header-cart-container">
	<a class="center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow">
		<div class="row flex">
			
			<div class="col-md-3 col-sm-12 col-xs-12 no-padding">
				<i class="fas fa-shopping-cart header-icons relative">
					<span class="JScart_num badge hidden-lg hidden-md">{{ Cart::broj_cart() }}</span>
				</i>
			</div>
			
			<div class="col-xs-9 hidden-sm hidden-xs">
				<div class="icon-txt text-left">
					<p>Vaša Korpa <span class="JScart_num badge">{{ Cart::broj_cart() }}</span></p> 
					<span class="JSmini-cart-total hidden-xs">{{ Cart::cena(Cart::cart_ukupno()) }}</span> 	
				</div>
				<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	        </div>
			  
		</div>
	</a>

	<div class="JSheader-cart-content hidden-sm hidden-xs">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
	
</div> 