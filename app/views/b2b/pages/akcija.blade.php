@extends('b2b.templates.main')
@section('content')
<div class="main-content">
    <div class="row">  
        <div class="col-md-12 col-sm-12 col-xs-12 cont-replace"> 
            <div class="row product-list-options">  <!-- ================= PRODUCT INFO OPTIONS ===================== -->
                <div class="col-md-7 col-sm-8 col-xs-12 sm-text-center"> 
                    <span>Ukupno: {{ $count_products }} </span>

                    @if(B2bOptions::product_sort()==1)
                        <div class="dropdown"> 
                            <button class="btn currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
                                {{B2bCommon::get_sort()}} 
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu currency-list">
                                @if(B2bOptions::web_options(207) == 0)
                                <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
                                <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
                                @else
                                <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
                                <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
                                @endif
                                <li><a href="{{route('b2b.ordering',['news'])}}">Najnovije</a></li>
                                <li><a href="{{route('b2b.ordering',['name'])}}">Prema nazivu</a></li>
                            </ul>
                        </div>
                    @endif 
                   
                    @if(B2bArticle::b2bproduct_view()==1)
                        <div class="view-buttons"> 
                            <a href="{{ B2bOptions::base_url() }}b2b/prikaz/table">
                                <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'table' OR !Session::has('b2b_prikaz')) ? 'active' : '' }}">
                                     <i class="fas fa-bars"></i> 
                                </span>
                            </a>
                            <a href="{{ B2bOptions::base_url() }}b2b/prikaz/grid">
                                <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'grid') ? 'active' : '' }}">
                                    <i class="fas fa-th"></i>
                                </span>
                            </a>       
                        </div>
                    @endif  
                </div>
                
  <!-- ========= PAGINATION ==============-->
                <div class="col-md-5 col-sm-4 col-xs-12 text-right sm-text-center"> 
                	{{ Paginator::make($query_products, $count_products, $limit)->links() }}
                </div>
            </div>  

            <div class="row">       <!-- ============ PRODUCT CONTENT ============== -->
                @if(Session::has('b2b_prikaz'))
                    @if(Session::get('b2b_prikaz') == 'table')
                        @include('b2b.partials/product_on_table')
                    @elseif(Session::get('b2b_prikaz') == 'grid')
	                    @foreach($query_products as $row)
	                        @include('b2b.partials/product_on_grid')
	                    @endforeach 
                    @endif   
                @else
                     @include('b2b.partials/product_on_table')       
                @endif

                @if($count_products == 0)
                    <p class="empty-page"> Trenutno nema artikla za date kategorije</p> 
                @endif 
            </div>
<!--========== PAGINATION DOWN ========= -->
            <div class="row text-right"> 
            	{{ Paginator::make($query_products, $count_products, $limit)->links() }}
            </div>
        </div>
    </div> 
</div>
@endsection