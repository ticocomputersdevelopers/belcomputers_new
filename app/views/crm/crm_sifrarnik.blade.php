@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif


<section id="main-content">
@include('crm/partials/crm_tabs')


<!-- AKCIJA TIP -->
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi tip akcije') }}</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}crm/crm_sifrarnik/0">{{ AdminLanguage::transAdmin('Dodaj novi tip akcije') }}</option>
						@foreach($crm_akcija_tip as $row) 
							<option value="{{ AdminOptions::base_url() }}crm/crm_sifrarnik/{{ $row->crm_akcija_tip_id }}" @if($row->crm_akcija_tip_id == $crm_akcija_tip_id) {{ 'selected' }} @endif> ({{ $row->crm_akcija_tip_id }}) {{ $row->naziv }} </option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>

				<!-- <h1 id="info"></h1> -->

				<form method="POST" action="{{ AdminOptions::base_url() }}crm/crm_sifrarnik" enctype="multipart/form-data">
					  	<input type="hidden" name="crm_akcija_tip_id" value="{{ $crm_akcija_tip_id }}">
					  	<div class="row">
							<div class="columns medium-12 ">
								<label for="naziv">{{ AdminLanguage::transAdmin('Naziv tipa akcije') }}</label>
								<input type="text" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
						</div>

					    <div class="row">
							<div class="columns medium-4 field-group">
								<label>{{ AdminLanguage::transAdmin('Aktivan') }}</label>
								<select name="flag_aktivan" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@if(Input::old('active'))
										@if(Input::old('active'))
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
										@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
										@endif
									@else
										@if($active)
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
										@else
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0">{{ AdminLanguage::transAdmin('NE') }}</option>
										@endif
									@endif
								</select>
							</div>
				
							<div class="columns medium-4 field-group">
								<label for="rbr">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
								<input type="text" name="rbr" value="{{ Input::old('rbr') ? Input::old('rbr') : $rbr }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
						</div>

					
							<div class="row"> 
								<div class="btn-container center">
									<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
									
									<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}crm/crm_sifrarnik/crm_sifrarnik-delete/{{ $crm_akcija_tip_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
				 					
								</div>
							</div>
						
					 
				</form>
			</div>
	</section>
</div> <!-- end of .flat-box -->

 