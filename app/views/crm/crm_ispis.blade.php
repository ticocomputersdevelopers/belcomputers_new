@if(Session::has('message'))
<script>
	alertify.success('{{ Session::get('message') }}');
</script>
@endif 

<div>
	<h1>{{ AdminCrm::nazivCrmPartnera($crm_id) }} <a href="{{AdminOptions::base_url()}}admin/kupci_partneri/partner/{{AdminCrm::getPartnerID($crm_id) }}" target="_blank" class="btn btn-secondary btn-small">{{ AdminLanguage::transAdmin('Vidi partnera') }} </a></h1>
	<div class="table-scroll"> 
		<table>		
			<thead> 
				<tr> 
					
					<th>{{ AdminLanguage::transAdmin('Datum pocetka CRM') }}</th>
					<th>{{ AdminLanguage::transAdmin('akcije') }}</th>
					<th>{{ AdminLanguage::transAdmin('napomene akcije') }}</th>
					<th>{{ AdminLanguage::transAdmin('Datum početka akcije') }}</th>
					<th>{{ AdminLanguage::transAdmin('Datum završetka akcije') }}</th>
					<th>{{ AdminLanguage::transAdmin('Datum zavrsetka CRM') }}</th>
					<th></th> 
				</tr>
			</thead>
			<tbody>

		@foreach($akcije as $row)
			<tr @if($row->flag_zavrseno) style="background-color:#ffb4b0" @endif>
					
					<td>{{date('d-m-Y, g:i a',strtotime($row->datum_kreiranja)) }}</td>  
					<td>{{ $row->naziv }} </td>
					<td class="no-white-space">{{ $row->opis }} </td> 
					@if(isset($row->datum)) 
					<td>{{date('d-m-Y, g:i a',strtotime($row->datum)) }}</td>
					@else
					<td></td>
					@endif 
					@if(isset($row->akcija_zavrsetak)) 
					<td>{{date('d-m-Y, g:i a',strtotime($row->akcija_zavrsetak))}}</td>
					@else
					<td></td>
					@endif
					@if(isset($row->datum_zavrsetka)) 
						<td>{{date('d-m-Y, g:i a',strtotime($row->datum_zavrsetka))}}</td>
					@else
					<td></td>  
					@endif
			</tr>				
		@endforeach
			</tbody>
		</table>
	</div> 
</div> 
