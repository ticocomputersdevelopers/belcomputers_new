@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif


<section id="main-content">
@include('crm/partials/crm_tabs')

<!-- vrsta -->			
 	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi vrstu') }}</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
							<option value="{{ AdminOptions::base_url() }}crm/crm_kontakt_vrsta/0">{{ AdminLanguage::transAdmin('Dodaj novu vrstu') }}</option>
						@foreach($vrsta_kontakta as $row) 
							<option value="{{ AdminOptions::base_url() }}crm/crm_kontakt_vrsta/{{ $row->vrsta_kontakta_id }}"@if($row->vrsta_kontakta_id == $vrsta_kontakta_id) {{ 'selected' }} @endif> {{ $row->naziv }} </option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<form method="POST" action="{{ AdminOptions::base_url() }}crm/crm_kontakt_vrsta" enctype="multipart/form-data">
					  	<input type="hidden" name="vrsta_kontakta_id"  value="{{ $vrsta_kontakta_id }}">
					  	<div class="row">
							<div class="columns medium-12 ">
								<label for="naziv">{{ AdminLanguage::transAdmin('Naziv vrste') }}</label>
								<input type="text" name="naziv"  value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
						</div>

						<div class="row"> 
								<div class="btn-container center">
									<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
									<button class="btn btn-danger JSbtn-delete"  data-link="{{ AdminOptions::base_url() }}crm/crm_kontakt_vrsta/{{$vrsta_kontakta_id}}/vrsta_delete">{{ AdminLanguage::transAdmin('Obriši') }}</button>	
								</div>
						</div>		 
				</form>
			</div>
	</section>
</div> <!-- end of .flat-box -->				
</section>