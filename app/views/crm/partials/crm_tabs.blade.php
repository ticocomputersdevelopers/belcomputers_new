 
<ul class="m-tabs">
	<li class="m-tabs__tab {{ $strana=='crm_home' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_home">{{ AdminLanguage::transAdmin('CRM') }}</a>
	</li>
	<li class="m-tabs__tab {{ $strana=='crm_sifrarnik' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_sifrarnik">{{ AdminLanguage::transAdmin('Tip Akcije') }}</a>
	</li>
	<li class="m-tabs__tab {{ $strana=='crm_status' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_status">{{ AdminLanguage::transAdmin('Statusi') }}</a>
	</li>
	<li class="m-tabs__tab {{ $strana=='crm_tip' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_tip">{{ AdminLanguage::transAdmin('Crm Tipovi') }}</a>
	</li>
	<li class="m-tabs__tab {{ $strana=='crm_kontakt_vrsta' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_kontakt_vrsta">{{ AdminLanguage::transAdmin('Vrste kontakata') }}</a>
	</li>
	<li class="m-tabs__tab {{ $strana=='crm_partneri' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_partneri">{{ AdminLanguage::transAdmin('Partneri') }}</a>
	</li>
	@if(AdminCrm::check_administrator())
	<li class="m-tabs__tab {{ $strana=='crm_analitika' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_analitika">{{ AdminLanguage::transAdmin('CRM Analitika') }}</a>
	</li>
	@endif
	@if(Admin_model::check_admin())
	<li class="m-tabs__tab {{ $strana=='crm_fakture' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_fakture">{{ AdminLanguage::transAdmin('Fakturisanje') }}</a>
	</li>
	<li class="m-tabs__tab {{ $strana=='crm_sertifikati' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_sertifikati">{{ AdminLanguage::transAdmin('Sertifikati') }}</a>
	</li>

	<li class="m-tabs__tab {{ $strana=='crm_taskovi' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_taskovi">{{ AdminLanguage::transAdmin('Taskovi') }}</a>
	</li>
	<li class="m-tabs__tab {{ $strana=='crm_logovi' ? ' m-tabs__tab--active' : '' }}">
		<a href="{{ AdminOptions::base_url() }}crm/crm_logovi">{{ AdminLanguage::transAdmin('Logovi') }}</a>
	</li>
	@endif	 
</ul>  