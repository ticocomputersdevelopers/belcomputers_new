<div class="row"> 
	<div class="napomena">
		<p>{{ AdminLanguage::transAdmin('Kod placanja - poziv na broj') }}: {{$ponuda->broj_dokumenta}}</p>
		<p><strong>{{ AdminLanguage::transAdmin('Napomena') }}:</strong></p>
		<p></p>
		<br><br>
	</div>
</div>

<div class="row">
	{{ $ponuda->tekst }}
	<br><br>
</div>

<div class="row"> 
	<table class="signature">
		<tr>
			<td class="">
				<p><strong>{{ AdminLanguage::transAdmin('Opcija ponude') }}:</strong></p>
				<p>_______________________________</p>				
			</td>
			<td class="text-right"><span class="robu_izdao">{{ AdminLanguage::transAdmin('Odgovorno lice') }}</span>___________________________</td>
		</tr>
	</table>
</div>

<div class="footer-comment">
	{{ $ponuda->tekst_footer }}
</div>
