
<div id="main-content" class="kupci-page">
    @if(Session::has('message'))
    <script>
        alertify.success('{{ Session::get('message') }}');
    </script>
    @endif

    <div class="row"> 
        <div class="large-2 medium-3 small-12 columns">
            <a class="m-subnav__link JS-nazad" href="{{ AdminOptions::base_url() }}admin/kupci_partneri/partner/{{$partner_id}}">
                <div class="m-subnav__link__icon">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </div>
                <div class="m-subnav__link__text">
                    {{ AdminLanguage::transAdmin('Nazad') }}
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <h3 class="title-med">
                Korisnici partnera - {{ AdminPartneri::partner($partner_id)->naziv }}
            </h3>
 
            <a class="btn btn-primary btn-small margin-bottom-buttons" href="{{ AdminOptions::base_url() }}admin/partner/{{$partner_id}}/korisnik/0">+ Dodaj novog</a>

            <table> 
                <thead> 
                    <tr> 
                        <th>Naziv</th>
                        <th>Adresa</th> 
                        <th>Mesto</th>
                        <th>Telefon</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($partnerKorisnici as $partnerKorisnik)
                    <tr> 
                        <td>{{$partnerKorisnik->naziv}}</td>
                        <td>{{$partnerKorisnik->adresa}}</td>
                        <td>{{$partnerKorisnik->mesto}}</td>
                        <td>{{$partnerKorisnik->telefon}}</td>
                        <td>{{$partnerKorisnik->email}}</td>
                        <td><a href="{{ AdminOptions::base_url() }}admin/partner/{{$partner_id}}/korisnik/{{ $partnerKorisnik->partner_korisnik_id }}">Vidi</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table> 

            <div>
                {{ $partnerKorisnici->links() }}
            </div>
        </div>
    </div>
</div>