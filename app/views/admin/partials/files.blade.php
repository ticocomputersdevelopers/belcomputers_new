<div id="JSFilesModal" class="reveal-modal large add-image" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodavanje fajlova') }}</h4>
	
	<div class="row">
		<div class="column large-3 medium-6 small-6">
			<div class="m-input-and-button options-div small-margin-bottom">
				<button id="JSSelectFilesAssignToTinyEditor" class="articles-btn-l a-btn-articles btn-admin btn btn-primary btn-xm imgs-btn">{{ AdminLanguage::transAdmin('Dodeli fajl') }}</button>
			</div>
		</div>

		<div class="column large-3 medium-6 small-6">
			<input type="file" name="files[]" id="JSFileUpload" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf" multiple> 
		</div>

		<div class="column large-1 medium-2 small-3 no-padd text-right">	
			<label class="no-margin"> {{ AdminLanguage::transAdmin('Pretraži') }} </label>
		</div>

		<div class="column large-5 medium-10 small-9">
			<input type="text" id="JSFileSearch" placeholder="{{ AdminLanguage::transAdmin('Naziv fajla') }}"> 
		</div>

		<div class="column large-12 medium-12 small-12">
			<div id="JSSelectedFiles" class="margin-bottom-buttons row">
				<span id="JSNotSelectFile" class="options-title-img column medium-12">{{ AdminLanguage::transAdmin('Niste izabrali fajl.') }}</span>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="column medium-12">
			<div class="JSFileContent"></div>
		</div>
	</div>
</div>