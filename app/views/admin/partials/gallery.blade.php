<div id="JSGalleryModal" class="reveal-modal large add-image" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodavanje slike') }}</h4>
	
	<div class="row">
		<div class="column large-3 medium-6 small-6">
			@if($strana == 'artikli' OR $strana == 'slike')
			<div class="m-input-and-button options-div small-margin-bottom">
				<button id="JSSelectGalleryImageAssign" class="articles-btn-l a-btn-articles btn-admin btn btn-primary btn-xm imgs-btn">{{ AdminLanguage::transAdmin('Dodeli sliku') }}</button>
			</div>
			@elseif($strana != 'artikli' AND $strana != 'slike')
			<div class="m-input-and-button options-div small-margin-bottom">
				<button id="JSSelectGalleryImageAssignToTinyEditor" class="articles-btn-l a-btn-articles btn-admin btn btn-primary btn-xm imgs-btn">{{ AdminLanguage::transAdmin('Dodeli sliku') }}</button>
			</div>
			@endif

		</div>

		<div class="column large-3 medium-6 small-6">
			<input type="file" name="slike[]" id="JSGalleryImageUpload" accept="image/*" multiple> 
		</div>

		<div class="column large-1 medium-2 small-3 no-padd text-right">	
			<label class="no-margin"> {{ AdminLanguage::transAdmin('Pretraži') }} </label>
		</div>

		<div class="column large-5 medium-10 small-9">
			<input type="text" id="JSGalleryImageSearch" placeholder="{{ AdminLanguage::transAdmin('Naziv fotografije') }}"> 
		</div>

		<div class="column large-12 medium-12 small-12">
			<div id="JSSelectedGalleryImages" class="margin-bottom-buttons row">
				<span id="JSGalleryNotSelectImage" class="options-title-img column medium-12">{{ AdminLanguage::transAdmin('Niste izabrali sliku.') }}</span>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="column medium-12 small-12">
			<div class="JSContent row"></div>
		</div>
	</div>
</div>