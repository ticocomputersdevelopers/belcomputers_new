<ul>
@foreach(DB::table('grupa_pr_naziv')->where(array('grupa_pr_id'=>(Input::old('grupa_pr_id') ? Input::old('grupa_pr_id') : $grupa_pr_id),'active'=>1))->orderBy('rbr','asc')->get() as $grupa_pr_naziv)
	<li>
		{{ $grupa_pr_naziv->naziv }} <span class="JSGrupaNazivRemove"><i class="fa fa-close tooltipz-left" aria-label="{{ AdminLanguage::transAdmin('Ukloni') }}"></i></span>
		<ul>
		@foreach(DB::table('grupa_pr_vrednost')->where(array('grupa_pr_naziv_id'=>$grupa_pr_naziv->grupa_pr_naziv_id,'active'=>1))->orderBy('rbr','asc')->get() as $grupa_pr_vrednost)
			<li>
			<input type="radio" name="grupa_pr_naziv_id_{{ $grupa_pr_naziv->grupa_pr_naziv_id }}" value="{{ $grupa_pr_vrednost->grupa_pr_vrednost_id }}" {{ in_array($grupa_pr_vrednost->grupa_pr_vrednost_id,$grupa_pr_vrednost_ids) ? 'checked="checked"' : '' }}>
				{{ $grupa_pr_vrednost->naziv }}
			</li>
		@endforeach
		</ul>
	</li>
@endforeach
</ul>