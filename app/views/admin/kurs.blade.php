<div id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	@if(Session::has('alert'))
	<script>
		alertify.error('{{ Session::get('alert') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">
		<section class="medium-4 columns">
			<h3 class="title-med">{{ AdminLanguage::transAdmin('Unos kursa') }} ({{ $default->valuta_sl }})</h3>
			<form method="POST" action="{{ AdminOptions::base_url() }}admin/kurs/edit">
				<div class="flat-box">	
					<div class="row"> 
						<div class="column medium-12 after-select-margin"> 
							<select name="valuta_id" id="JSkursSelect">
								@foreach($valute as $row)
								<option value="{{ $row->valuta_id }}" {{ $row->valuta_id == $valuta_id ? 'selected' : '' }}>{{ $row->valuta_naziv }}</option>
								@endforeach
							</select> 
						</div>
					</div>
				</div>

				<div class="flat-box">	

					<h3 class="title-med">{{ AdminLanguage::transAdmin('Današnji kurs') }}</h3>

					<section class="medium-6 columns">
						<div class="{{ $errors->first('kupovni') ? 'error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Kupovni') }}</label>
							<input type="text" name="kupovni" value="{{ Input::old('kupovni')!=null ? Input::old('kupovni') : $tekuca_valuta->kupovni }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="{{ $errors->first('srednji') ? 'error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Srednji') }}</label>
							<input type="text" name="srednji" value="{{ Input::old('srednji')!=null ? Input::old('srednji') : $tekuca_valuta->srednji }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="{{ $errors->first('prodajni') ? 'error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Prodajni') }}</label>
							<input type="text" name="prodajni" value="{{ Input::old('prodajni')!=null ? Input::old('prodajni') : $tekuca_valuta->prodajni }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</section>

					<section class="medium-6 columns">
						<div class="{{ $errors->first('ziralni') ? 'error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Kurs za import') }}</label>
							<input type="text" name="ziralni" value="{{ Input::old('ziralni')!=null ? Input::old('ziralni') : $tekuca_valuta->ziralni }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="{{ $errors->first('web') ? 'error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Web') }}</label>
							<input type="text" name="web" value="{{ Input::old('web')!=null ? Input::old('web') : $tekuca_valuta->web }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</section>

					<section class="columns">
						<div class="btn-container"> 
							@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							@endif

							@if($default->valuta_sl == 'RSD')
							<a href="{{ AdminOptions::base_url() }}admin/kurs/{{ $valuta_id }}/nbs" class="btn btn-primary">{{ AdminLanguage::transAdmin('Preuzmi od NBS') }}</a>
							@endif

							<a href="{{ AdminOptions::base_url() }}admin/kurs/{{ $valuta_id }}" class="fr btn btn-danger">{{ AdminLanguage::transAdmin('Otkaži') }}</a>
						</div> 
					</section>
				</div>
			</form>
		</section>

		<section class="medium-8 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Kursna lista po danima') }}</h3>
				<div class="table-scroll">
					<table class="fixed-table-header">
						<thead>
							<th>{{ AdminLanguage::transAdmin('Kurs ID') }}</th>
							<th>{{ AdminLanguage::transAdmin('Valuta') }}</th>
							<th>{{ AdminLanguage::transAdmin('Datum') }}</th>
							<th>{{ AdminLanguage::transAdmin('Kupovni') }}</th>
							<th>{{ AdminLanguage::transAdmin('Srednji') }}</th>
							<th>{{ AdminLanguage::transAdmin('Prodajni') }}</th>
							<th>{{ AdminLanguage::transAdmin('Žiralni') }}</th>
							<th>{{ AdminLanguage::transAdmin('Web') }}</th>
						</thead>
						<tbody>
							@foreach($tabela_valuta as $row)
							<tr>
								<td>{{ $row->kursna_lista_id }}</td>
								<td>{{ AdminSupport::getCurrencyName($row->valuta_id) }}</td>
								<td>{{ $row->datum }}</td>
								<td>{{ $row->kupovni }}</td>
								<td>{{ $row->srednji }}</td>
								<td>{{ $row->prodajni }}</td>
								<td>{{ $row->ziralni }}</td>
								<td>{{ $row->web }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div> 
</div>