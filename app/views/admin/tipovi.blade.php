<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">

		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi tip') }}</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/tip/0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
						@foreach(AdminSupport::getTipovi(true) as $row)
						<option value="{{ AdminOptions::base_url() }}admin/tip/{{ $row->tip_artikla_id }}" @if($row->tip_artikla_id == $tip_artikla_id) {{ 'selected' }} @endif>({{ $row->tip_artikla_id }}) {{ $row->naziv }} {{$row->rbr}}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="medium-5 columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<!-- <h1 id="info"></h1> -->

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/tip-edit" enctype="multipart/form-data">
					<input type="hidden" name="tip_artikla_id" value="{{ $tip_artikla_id }}">

					<div class="row">
						<div class="columns {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv tipa') }}</label>
							<input type="text" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>

					<div class="row">

						<div class="columns medium-4">
							<label>{{ AdminLanguage::transAdmin('Aktivan') }}</label>
							<select name="active" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if(Input::old('active'))
								@if(Input::old('active'))
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
								@else
								@if($active)
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								@endif
								@endif
							</select>
						</div>

						<div class="columns medium-4">
							<label>{{ AdminLanguage::transAdmin('Prikaz na: ') }}</label>

							<select name="prikaz" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::prikaz(Input::old('prikaz')  ? Input::old('prikaz')  : $prikaz) }}
							</select>
						</div>

						<div class="columns medium-4 {{ $errors->first('rbr') ? ' error' : '' }}">
							<label for="rbr">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
							<input type="text" name="rbr" value="{{ Input::old('rbr') ? Input::old('rbr') : $rbr }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($tip_artikla_id != 0)
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/tip-delete/{{ $tip_artikla_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
						@endif
					</div> 
					@endif

				</form>
			</div>
		</section>
	</div>  
</div>

