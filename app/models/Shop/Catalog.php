<?php

class Catalog {

    // public static function getCatalogs()
    // {

    //     return DB::table('katalog')->select('katalog_id','naziv')->where('aktivan',1)->where('katalog_id','<>',-1)->whereRaw("(select count(katalog_polja_id) from katalog_polja where katalog_id=katalog.katalog_id) > 0")->where('katalog_id','<>',0)->orderBy('naziv','asc')->get();
          
    // }

    public static function articlesCount($grupa_pr_id,$katalog_id){
        $gr=array();
        AdminCommon::allGroups($gr,$grupa_pr_id);
        $queryOsnovni=DB::table('roba')
                ->select([DB::RAW('DISTINCT(roba.roba_id)')]);
             
        $queryOsnovni->whereIn('grupa_pr_id', $gr)
                ->where(array('flag_aktivan'=>1, 'flag_prikazi_u_cenovniku'=>1))
                ->whereIn('roba_id',array_map('current',DB::table('katalog_roba')->where('katalog_id',$katalog_id)->select('roba_id')->get()));

        return count($queryOsnovni->get());     
    }
    public static function mainGroupArticlesCount($grupa_pr_id,$katalog_id){
        $queryOsnovni=DB::table('roba')
                ->select([DB::RAW('DISTINCT(roba.roba_id)')]);
             
        $queryOsnovni->where('grupa_pr_id', $grupa_pr_id)
                ->where(array('flag_aktivan'=>1, 'flag_prikazi_u_cenovniku'=>1))
                ->whereIn('roba_id',array_map('current',DB::table('katalog_roba')->where('katalog_id',$katalog_id)->select('roba_id')->get()));

        return count($queryOsnovni->get());     
    }

    public static function groupArtcles($katalog_id,$grupa_pr_ids){
        $sort = DB::table('katalog')->where('katalog_id',$katalog_id)->pluck('sort');
        $sortArr = explode('-',$sort);
        
        return DB::select("select r.* from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (1,2) and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id in (".implode(',',$grupa_pr_ids).") order by (select naziv from proizvodjac where proizvodjac_id = r.proizvodjac_id) asc, ".$sortArr[0]." ".$sortArr[1]);
    }









    public static function articlesKarakCount($grupa_pr_id,$katalog_id){
        $katalog_vrsta_ids = array(3);
        // $katalogGrupeSort = DB::select("select grupa_pr_naziv_id from katalog_grupe_sort where katalog_id = ".$katalog_id." and grupa_pr_id = ".$grupa_pr_id." limit 1");
        // if(count($katalogGrupeSort) == 0){
        //     return 0;
        // }
        $gr=array();
        AdminCommon::allGroups($gr,$grupa_pr_id);
        return count(DB::select("select distinct r.roba_id from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id in (".implode(',',$gr).")"));      
    }
    // public static function mainGroupArticlesKarakCount($grupa_pr_id,$katalog_id){
    //     $katalog_vrsta_ids = array(3);
    //     $katalogGrupeSort = DB::select("select grupa_pr_naziv_id from katalog_grupe_sort where katalog_id = ".$katalog_id." and grupa_pr_id = ".$grupa_pr_id." limit 1");
    //     if(count($katalogGrupeSort) == 0){
    //         return array();
    //     }
    //     $gr=array();
    //     AdminCommon::allGroups($gr,$grupa_pr_id);
    //     return count(DB::select("select r.* from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id left join web_roba_karakteristike wrk on wrk.roba_id = r.roba_id left join grupa_pr_vrednost gpv on gpv.grupa_pr_vrednost_id = wrk.grupa_pr_vrednost_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id in ".implode(',',$gr)." and gpv.grupa_pr_naziv_id = ".$katalogGrupeSort[0]->grupa_pr_naziv_id.""));  
    // }
    public static function karakteristikaNaziv($katalog_id,$grupa_pr_id){
        $karakteristikanaziv = DB::select("select (select naziv from grupa_pr_naziv where grupa_pr_naziv_id = kgs.grupa_pr_naziv_id) from katalog_grupe_sort kgs where katalog_id = ".$katalog_id." and grupa_pr_id = ".$grupa_pr_id." limit 1");
        if(count($karakteristikanaziv) == 0){
            return null;
        }
        return $karakteristikanaziv[0]->naziv;
    }
    public static function availableManufaturers($katalog_id,$grupa_pr_id){
        $katalog_vrsta_ids = array(3);

        return DB::select("select distinct r.proizvodjac_id, (select naziv from proizvodjac where proizvodjac_id = r.proizvodjac_id limit 1) as proizvodjac_naziv, (select rbr from proizvodjac where proizvodjac_id = r.proizvodjac_id limit 1) from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id = ".$grupa_pr_id." order by (select rbr from proizvodjac where proizvodjac_id = r.proizvodjac_id limit 1) asc");        
    }

    public static function availableCharacteristicts($katalog_id,$grupa_pr_id,$proizvodjac_id){
        $katalog_vrsta_ids = array(3);
        $katalogGrupeSort = DB::select("select grupa_pr_naziv_id from katalog_grupe_sort where katalog_id = ".$katalog_id." and grupa_pr_id = ".$grupa_pr_id." limit 1");
        if(count($katalogGrupeSort) == 0){
            return array();
        }
        return DB::select("select distinct gpv.grupa_pr_vrednost_id, gpv.naziv, gpv.rbr from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id left join web_roba_karakteristike wrk on wrk.roba_id = r.roba_id left join grupa_pr_vrednost gpv on gpv.grupa_pr_vrednost_id = wrk.grupa_pr_vrednost_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id = ".$grupa_pr_id." and r.proizvodjac_id = ".$proizvodjac_id." and gpv.grupa_pr_naziv_id = ".$katalogGrupeSort[0]->grupa_pr_naziv_id." order by gpv.rbr asc");
    }

    public static function groupKarakArtcles($katalog_id,$grupa_pr_id,$proizvodjac_id,$grupa_pr_vrednost_id=null){
        $katalog_vrsta_ids = array(3);
        $sort = DB::table('katalog')->where('katalog_id',$katalog_id)->pluck('sort');
        $sortArr = explode('-',$sort);

        $grupa_pr_vrednost_ids = array();
        if(!is_null($grupa_pr_vrednost_id)){
            $grupa_pr_vrednost_ids = array((object) array('grupa_pr_vrednost_id'=>$grupa_pr_vrednost_id));
        }else{
            $grupa_pr_vrednost_ids = DB::select("select distinct gpv.grupa_pr_vrednost_id from katalog_grupe_sort kgs left join grupa_pr_vrednost gpv on gpv.grupa_pr_naziv_id = kgs.grupa_pr_naziv_id where katalog_id = ".$katalog_id." and grupa_pr_id = ".$grupa_pr_id."");
            if(count($grupa_pr_vrednost_ids) == 0){
                return array();
            }
        }
        return DB::select("select distinct r.* from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id inner join web_roba_karakteristike wrk on wrk.roba_id = r.roba_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id = ".$grupa_pr_id." and r.proizvodjac_id = ".$proizvodjac_id." and wrk.grupa_pr_vrednost_id in (".implode(',',array_map('current',$grupa_pr_vrednost_ids)).") order by ".$sortArr[0]." ".$sortArr[1]);
    }

    public static function groupWithoutKarakArtcles($katalog_id,$grupa_pr_id,$proizvodjac_id){
        $katalog_vrsta_ids = array(3);
        $sort = DB::table('katalog')->where('katalog_id',$katalog_id)->pluck('sort');
        $sortArr = explode('-',$sort);

        $grupa_pr_vrednost_ids = DB::select("select distinct gpv.grupa_pr_vrednost_id from katalog_grupe_sort kgs left join grupa_pr_vrednost gpv on gpv.grupa_pr_naziv_id = kgs.grupa_pr_naziv_id where katalog_id = ".$katalog_id." and grupa_pr_id = ".$grupa_pr_id."");

        return DB::select("select distinct r.* from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id = ".$grupa_pr_id." and r.proizvodjac_id = ".$proizvodjac_id."".(count($grupa_pr_vrednost_ids) > 0 ? " and r.roba_id not in (select distinct r.roba_id from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id inner join web_roba_karakteristike wrk on wrk.roba_id = r.roba_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id = ".$grupa_pr_id." and r.proizvodjac_id = ".$proizvodjac_id." and wrk.grupa_pr_vrednost_id in (".implode(',',array_map('current',$grupa_pr_vrednost_ids))."))":"")." order by ".$sortArr[0]." ".$sortArr[1]);

    }
    public static function groupKarakArtclesAll($katalog_id,$grupa_pr_id,$proizvodjac_id){
        $katalog_vrsta_ids = array(3);
        $sort = DB::table('katalog')->where('katalog_id',$katalog_id)->pluck('sort');
        $sortArr = explode('-',$sort);

        $grupa_pr_vrednost_ids = DB::select("select distinct gpv.grupa_pr_vrednost_id from katalog_grupe_sort kgs left join grupa_pr_vrednost gpv on gpv.grupa_pr_naziv_id = kgs.grupa_pr_naziv_id where katalog_id = ".$katalog_id." and grupa_pr_id = ".$grupa_pr_id."");

        return DB::select("select distinct r.* from roba r right join katalog_roba kr on kr.roba_id = r.roba_id left join katalog k on k.katalog_id = kr.katalog_id where kr.katalog_id = ".$katalog_id." and k.katalog_vrsta_id in (".implode(',',$katalog_vrsta_ids).") and flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and grupa_pr_id = ".$grupa_pr_id." and r.proizvodjac_id = ".$proizvodjac_id." order by ".$sortArr[0]." ".$sortArr[1]);

    }





    public static function types(){
        return DB::table('tip_artikla')->get();
    }

    public static function typeArtcles($katalog_id,$tip_id){
        $sort = DB::table('katalog')->where('katalog_id',$katalog_id)->pluck('sort');
        $sortArr = explode('-',$sort);
        
        return DB::table('roba')->select('roba.*')->rightJoin('katalog_roba','katalog_roba.roba_id','=','roba.roba_id')->where('katalog_roba.katalog_id',$katalog_id)->where(array('flag_aktivan'=>1, 'flag_prikazi_u_cenovniku'=>1))->where('tip_cene',$tip_id)->orderBy($sortArr[0],$sortArr[1])->get();
    }

}