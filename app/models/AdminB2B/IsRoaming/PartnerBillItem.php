<?php
namespace IsRoaming;

use DB;

class PartnerBillItem {

    public static function table_body($partners_card_items,array $getMappedBills,array $mappedArticles){
        $result_arr = array();
        $racun_stavka_id = DB::select("SELECT nextval('racun_stavka_racun_stavka_id_seq1')")[0]->nextval;

        foreach($partners_card_items as $item) {
            $racun_id = isset($getMappedBills[strval($item->opis)]) ? $getMappedBills[strval($item->opis)] : null;
            $roba_id = isset($mappedArticles[strval($item->artikal)]) ? $mappedArticles[strval($item->artikal)] : null;


            if(!is_null($racun_id) && !is_null($roba_id)){

                $racun_stavka_id++;
                $redni_broj = strlen($item->stavka) > 4 ? substr($item->stavka,0,-4) : '0';
                $kolicina = isset($item->kolicina) ? $item->kolicina : 0.00;
                $cena = isset($item->vrednost) ? $item->vrednost : 0.00;
                $cena_nc = $cena / 1.2;

                $result_arr[] = "(".strval($racun_stavka_id).",".strval($racun_id).",".$redni_broj.",".strval($roba_id).",".$cena_nc.",20.00,".$cena.",".strval($kolicina).",NULL)";
            }
        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function query_insert_update($table_temp_body,$upd_cols=array()) {
        if($table_temp_body == ''){
            return false;
        }
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='racun_stavka'"));
        $table_temp = "(VALUES ".$table_temp_body.") racun_stavka_temp(".implode(',',$columns).")";

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="racun_stavka_id" && $col!="racun_id"){
                $updated_columns[] = "".$col." = racun_stavka_temp.".$col."";
            }
        }

        //update
        // DB::statement("UPDATE racun_stavka SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE (racun_stavka_temp.racun_id,racun_stavka_temp.vrsta_dokumenta,racun_stavka_temp.datum_dokumenta,racun_stavka_temp.opis,racun_stavka_temp.duguje,racun_stavka_temp.potrazuje) IN (SELECT partner_id,vrsta_dokumenta,datum_dokumenta,opis,duguje,potrazuje FROM racun_stavka)");

        //DELETE
        DB::statement("DELETE FROM racun_stavka WHERE (racun_id,roba_id,pcena) NOT IN (SELECT racun_id,roba_id,pcena FROM ".$table_temp.")");

        //insert
        DB::statement("INSERT INTO racun_stavka (SELECT * FROM ".$table_temp." WHERE (racun_id,roba_id,pcena) NOT IN (SELECT racun_id,roba_id,pcena FROM racun_stavka))");

        DB::statement("SELECT setval('racun_stavka_racun_stavka_id_seq1', (SELECT MAX(racun_stavka_id) FROM racun_stavka), FALSE)");
    }

    public static function query_delete_unexists($table_temp_body) {
        //
    }
}
