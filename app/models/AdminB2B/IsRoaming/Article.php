<?php
namespace IsRoaming;
use DB;

class Article {

	public static function table_body($articles){
		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			if(isset($article->sifra_artikla)){
				$id_is = $article->sifra_artikla;
				$roba_id++;
				$sifra_k++;
				
				$sifra_is = $id_is;
				$naziv = isset($article->naziv) ? pg_escape_string(Support::convert(substr($article->naziv,0,300))) : '';
				$web_opis = isset($article->opis) ? pg_escape_string(nl2br(htmlspecialchars(Support::convert($article->opis)))) : '';
				$barkod = isset($article->barkod) ? Support::convert($article->barkod) : '';
				$proizvodjac_id = isset($article->proizvodjac) && trim($article->proizvodjac) != '' ? Support::getProizvodjacId(Support::convert($article->proizvodjac)) : -1;
				$vrednost_tarifne_grupe = isset($article->pdv) && is_numeric($article->pdv) ? $article->pdv : 20;
				$tarifna_grupa_id = Support::getTarifnaGrupaId('Opšta',$vrednost_tarifne_grupe);
				// $tarifna_grupa_id = isset($article->tarifna_grupa) ? Support::getTarifnaGrupaId($article->tarifna_grupa,$vrednost_tarifne_grupe) : 0;
				// $jedinica_mere_id = isset($article->jedinica_mere) && $article->jedinica_mere != '' ? Support::getJedinicaMereId($article->jedinica_mere) : 1;
				// $grupa_pr_id = isset($article->grupa) && $article->grupa != '' ? Support::getGrupaId($article->grupa) : -1;
				// $racunska_cena_nc = isset($article->cena_nc) && is_numeric(floatval($article->cena_nc)) ? floatval($article->cena_nc) : 0;
				// $web_cena = isset($article->web_cena) && is_numeric(floatval($article->web_cena)) ? floatval($article->web_cena) : 0;
				// $akcijska_cena = isset($article->akcijska_cena) && is_numeric(floatval($article->akcijska_cena)) ? floatval($article->akcijska_cena) : 0;
				// $akcija_flag_primeni = isset($article->akcija_aktivna) && in_array($article->akcija_aktivna,array(1,0)) ? intval($article->akcija_aktivna) : 0;

				$jedinica_mere_id = 1;
				$akcija_flag_primeni = 0;
				$grupa_pr_id = -1;
				$racunska_cena_nc = 0;
				$web_cena = 0;
				$akcijska_cena = 0;
				$akcija_flag_primeni = 0;
				// $mpcena = isset($article->mpcena) && is_numeric(floatval($article->mpcena)) ? floatval($article->mpcena) : 0;
				$mpcena = isset($article->mpcena) && is_numeric(floatval($article->mpcena)) ? floatval($article->mpcena) : 0;
				$flag_aktivan = '0';
				$flag_prikazi_u_cenovniku = '0';

				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_prikazi_u_cenovniku.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,".strval($akcija_flag_primeni).",0,NULL,NULL,NULL,NULL,1,0,'".$barkod."',0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,".strval($akcijska_cena).",0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."')";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		//update
		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar");

		//update unactive
		DB::statement("UPDATE roba t SET flag_aktivan = 1 FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.flag_aktivan = 0 AND t.grupa_pr_id > 0");

		//insert
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba))");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0, mpcena = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}