<?php
namespace IsInfograf;

use DB;
use Image;
use finfo;

class Support {

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[strval($article->id_is)] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[strval($group->id_is)] = $group->grupa_pr_id;
		}
		return $mapped;
	}
	public static function getMappedManufacturers(){
		$mapped = array();
		$manufacturers = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($manufacturers as $manufacturer){
			$mapped[strval($manufacturer->id_is)] = $manufacturer->grupa_pr_id;
		}
		return $mapped;
	}
	public static function getMappedPartners(array $id_is = array()){
		$mapped = array();
		if(count($id_is) > 0){
			$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->whereIn('id_is',$id_is)->get();
		}else{
			$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->get();
		}
		foreach($partners as $partner){
			$mapped[strval($partner->id_is)] = $partner->partner_id;
		}
		return $mapped;
	}

	public static function updateGroupsParent($groups){
		foreach($groups as $group){
			$group_id = intval($group->id);
			$parent_id = isset($group->parent_id) && $group->parent_id != 0 ? intval($group->parent_id) : 0;
			if($parent_id != 0){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('id_is',$parent_id)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					if($group->LEVEL >= 5){
						$parrent_grupa_pr_id = DB::table('grupa_pr')->where('grupa_pr_id',$parrent_grupa_pr_id)->pluck('parrent_grupa_pr_id');
					}
					DB::table('grupa_pr')->where(array('id_is'=>$group_id))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}else{
					DB::table('grupa_pr')->where('parrent_grupa_pr_id',$parent_id)->delete();
				}
			}
		}

	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getGrupaId($is_grupa_id){
		$grupa_pr = DB::table('grupa_pr')->where('id_is',intval($is_grupa_id))->first();
		if(!is_null($grupa_pr)){
			return $grupa_pr->grupa_pr_id;
		}
		return -1;
	}

	public static function getManufacturerId($manufacturer_id){
		$proizvodjac = DB::table('proizvodjac')->where('id_is',$manufacturer_id)->first();
		if(!is_null($proizvodjac)){
			return $proizvodjac->proizvodjac_id;
		}
		return -1;
	}

	public static function convert($string){
		return str_replace(array('è','æ','ð','Æ','È'), array('č','ć','đ','Ć','Č'), $string);
	}

	public static function image_type($content){
		try {
			if(class_exists('finfo')){
			    $finfo    = new finfo(FILEINFO_MIME);
			    $mimetype = $finfo->buffer($content);
			}else{
			    $mimetype = "application/octet-stream";
			}
			
			$mimetypeinfoArr = explode(';',$mimetype);
			if(isset($mimetypeinfoArr[0])){
				$typeArr = explode('/',$mimetypeinfoArr[0]);
				if(isset($typeArr[1]) && in_array($typeArr[1],array('png','jpg','jpeg'))){
					return $typeArr[1];
				}
			}
		} catch (Exception $e) {
			return null;
		}
		return null;
	}

	public static function saveImageFile($putanja,$content,$sirina_big=600){
		try {
			$img_file = fopen($putanja,"w");
			fwrite($img_file, $content);
			fclose($img_file);

			if(filesize($putanja) <= 1000000){
				Image::make($putanja)->resize($sirina_big, null, function ($constraint) { $constraint->aspectRatio(); })->save();
			}
			return true;
		} catch (Exception $e) {
			File::delete($putanja);
			return false;
		}
	}
	public static function roba_id_from_sifra($sifra){
		return DB::table('roba')->where('sifra_is',$sifra)->pluck('roba_id');
	}
	public static function save_image_files($images,$new_images){
		$sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');
		foreach($images as $image){
			if(isset($new_images[$image->id])){
				self::saveImageFile($new_images[$image->id],$image->slika,$sirina_big);
			}
		}
	}

}