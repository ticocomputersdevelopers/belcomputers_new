<?php
namespace IsSpSoft;

use DB;


class PartnerRabat {

	public static function table_body($cards,$mappedPartners,$mappedGroups){
        $result_arr = array();
        $id=0;
        foreach($cards as $item) {
            $partner_sifra = trim($item->acSubject);
            if(!empty($partner_sifra)){            
                $partner_id = isset($mappedPartners[$partner_sifra]) ? $mappedPartners[$partner_sifra] : null;
                if(!is_null($partner_id) && (empty(trim($item->acClassif)) || isset($mappedGroups[$item->acClassif]))){
                    if(empty(trim($item->acClassif))){
                        $grupa_pr_id = -1;
                    }else{
                        $grupa_pr_id = $mappedGroups[$item->acClassif];
                    }
           
                    $id++;
                    $rabat = isset($item->anRebate) && is_numeric($item->anRebate) ? floatval($item->anRebate) : 0;
                    $aktivno = isset($item->acActive) && trim($item->acActive) == 'T' ? 1 : 0;
                    $datum_od = isset($item->adDateStart) && !empty($item->adDateStart) ? "'".date('Y-m-d',strtotime($item->adDateStart))."'" : '(NULL)::date';
                    $datum_do = isset($item->adDateEnd) && !empty($item->adDateEnd) ? "'".date('Y-m-d',strtotime($item->adDateEnd))."'" : '(NULL)::date';


    	            $result_arr[] = "(".strval($partner_id).",".strval($grupa_pr_id).",".strval($rabat).",(NULL)::integer,(NULL)::integer,-1,(NULL)::integer,".strval($id).",".strval($aktivno).",".strval($datum_od).",".strval($datum_do).")";
    	        }
            }
        }

        return (object) array("body"=>implode(",",$result_arr), "mapped_result"=>$mappedPartners);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner_rabat_grupa'"));
        $table_temp = "(VALUES ".$table_temp_body.") partner_rabat_grupa_temp(".implode(',',$columns).")";

        // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="id"){
                $updated_columns[] = "".$col." = partner_rabat_grupa_temp.".$col."";
            }
        }

        //update
        DB::statement("UPDATE partner_rabat_grupa prg SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE prg.partner_id=partner_rabat_grupa_temp.partner_id AND prg.grupa_pr_id=partner_rabat_grupa_temp.grupa_pr_id AND prg.proizvodjac_id=partner_rabat_grupa_temp.proizvodjac_id");

        //insert
        DB::statement("INSERT INTO partner_rabat_grupa(partner_id,grupa_pr_id,rabat,proizvodjac_id,aktivno,datum_od,datum_do) SELECT partner_id,grupa_pr_id,rabat,proizvodjac_id,aktivno,datum_od,datum_do FROM ".$table_temp." WHERE (partner_rabat_grupa_temp.partner_id, partner_rabat_grupa_temp.grupa_pr_id, partner_rabat_grupa_temp.proizvodjac_id) NOT IN (SELECT partner_id, grupa_pr_id, proizvodjac_id FROM partner_rabat_grupa)");

        // DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

	}

    public static function query_delete_unexists($table_temp_body) {
        if($table_temp_body == ''){
            return false;
        }
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner_rabat_grupa'"));
        $table_temp = "(VALUES ".$table_temp_body.") partner_rabat_grupa_temp(".implode(',',$columns).")";

        DB::statement("DELETE FROM partner_rabat_grupa WHERE (partner_id,grupa_pr_id,proizvodjac_id) NOT IN (SELECT partner_id,grupa_pr_id,proizvodjac_id FROM ".$table_temp.")");

    }

}
