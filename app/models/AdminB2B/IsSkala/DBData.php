<?php
namespace IsSkala;

use Config;
use DB;

class DBData {
	private $conn;

	public function __construct(){
		Config::set('database.connections.skala', array(
	        'driver'    => 'mysql',
	        'host'      => 'skala.rs',
	        'database'  => 'skalabgd_baza',
	        'username'  => 'skalabgd_readme',
	        'password'  => 'skala.read.only.password.123',
	        'charset'   => 'utf8',
	        'collation' => 'utf8_unicode_ci',
	        'prefix'    => '',
	    ));
	    $this->conn = DB::connection('skala');
	}

	public function groups(){
		$kategorije = $this->conn->select("SELECT * FROM kategorije");
		$podkategorije = $this->conn->select("SELECT * FROM podkategorije");
		$groups = array();
		foreach($kategorije as $kategorija){
			$groups[] = (object) array('sifra'=>$kategorija->id_kategorije,'naziv'=>$kategorija->naziv_kategorije,'parent'=>0,'rbr'=>$kategorija->red);
		}
		foreach($podkategorije as $podkategorija){
			$groups[] = (object) array('sifra'=>$podkategorija->id_kategorije.'-'.$podkategorija->id_podkategorije,'naziv'=>$podkategorija->naziv_podkategorije,'parent'=>$podkategorija->id_kategorije,'rbr'=>$podkategorija->red);
		}
		return $groups;
	}

	public function manufacturers(){
		return $this->conn->select("SELECT * FROM proizvodjaci");
	}

	public function articles(){
		return $this->conn->select("SELECT * FROM proizvodi p LEFT JOIN proizvodjaci pdj ON p.id_proizvodjaca = pdj.id_proizvodjaca");
		//(SELECT (naziv_kategorije || '-->' || naziv_podkategorije) FROM podkategorizovani ppk LEFT JOIN podkategorije pk ON ppk.id_podkategorije = pk.id_podkategorije LEFT JOIN kategorije k ON pk.id_kategorije = k.id_kategorije WHERE p.id_proizvoda = ppk.id_proizvoda LIMIT 1) AS kategorija
	}

	public function articles_groups(){
		$main_articles_groups = $this->conn->select("SELECT id_proizvoda, id_podkategorije FROM podkategorizovani GROUP BY id_proizvoda HAVING (min(id_podkategorije))");
		$secondary_articles_groups = $this->conn->select("SELECT * FROM podkategorizovani WHERE (id_proizvoda, id_podkategorije) NOT IN (SELECT id_proizvoda, id_podkategorije FROM podkategorizovani GROUP BY id_proizvoda HAVING (min(id_podkategorije)))");
		return (object) array('main_articles_groups'=>$main_articles_groups,'secondary_articles_groups'=>$secondary_articles_groups);
	}

	public function images(){
		return $this->conn->select("SELECT * FROM slike");
	}

	public function courses(){
		return $this->conn->select("SELECT * FROM kurs");
	}
}