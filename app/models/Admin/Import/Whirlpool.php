<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Whirlpool {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/whirlpool/whirlpool_excel/whirlpool.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('N'.$row)->getValue();
	            $naziv = $worksheet->getCell('C'.$row)->getValue();
	            $podgrupa = $worksheet->getCell('A'.$row)->getValue();
	            $opis = $worksheet->getCell('M'.$row)->getValue();

				if(!empty($naziv) && $naziv=='model'){

					$grupa=$podgrupa;
				}
				else if(!empty($sifra) && $naziv!='model'){

		            $cena = $worksheet->getCell('E'.$row)->getValue();
		            $rabat = $worksheet->getCell('F'.$row)->getValue();
		            $ncena = floatval($cena)*(1-floatval(str_replace('%','',$rabat)));
		            $a_rabat = $worksheet->getCell('G'.$row)->getValue();

					if(isset($a_rabat) && is_numeric(str_replace('%','',$a_rabat))){
			            $h_rabat = $worksheet->getCell('H'.$row)->getValue();
			            $ncena = floatval($cena)*(1-floatval(str_replace('%','',$rabat)))*(1-floatval(str_replace('%','',$a_rabat)))*(1-floatval(str_replace('%','',$h_rabat)));
					}

					$pmpcena = $worksheet->getCell('K'.$row)->getValue();
					$akcijska_pmpcena = $worksheet->getCell('L'.$row)->getValue();
					if(isset($akcijska_pmpcena) && is_numeric($akcijska_pmpcena)){
						$pmpcena=$akcijska_pmpcena;
					}
				
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250("Whirlpool ".$naziv)) . "',";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
					$sPolja .= " opis,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($opis)) . "',";
					$sPolja .= " podgrupa,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($ncena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "pmp_cena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}

			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/whirlpool/whirlpool_excel/whirlpool.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('N'.$row)->getValue();
	            $naziv = $worksheet->getCell('C'.$row)->getValue();

				if(!empty($naziv) && $naziv=='model'){

				}
				else if(!empty($sifra) && $naziv!='model'){

		            $cena = $worksheet->getCell('E'.$row)->getValue();
		            $rabat = $worksheet->getCell('F'.$row)->getValue();
		            $ncena = floatval($cena)*(1-floatval(str_replace('%','',$rabat)));
		            $a_rabat = $worksheet->getCell('G'.$row)->getValue();

					if(isset($a_rabat) && is_numeric(str_replace('%','',$a_rabat))){
			            $h_rabat = $worksheet->getCell('H'.$row)->getValue();
			            $ncena = floatval($cena)*(1-floatval(str_replace('%','',$rabat)))*(1-floatval(str_replace('%','',$a_rabat)))*(1-floatval(str_replace('%','',$h_rabat)));
					}
					
					$pmpcena = $worksheet->getCell('K'.$row)->getValue();
					$akcijska_pmpcena = $worksheet->getCell('L'.$row)->getValue();
					if(isset($akcijska_pmpcena) && is_numeric($akcijska_pmpcena)){
						$pmpcena=$akcijska_pmpcena;
					}
				
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($ncena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "pmp_cena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}