<?php
namespace Import;
use Import\Support;
use DB;
use File;
use AdminOptions;

class Printcom {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			self::autoDownload();
			//Support::autoDownload(Support::autoLink($dobavljac_id),'files/printcom/printcom.json');
			$products_file = "files/printcom/printcom.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;	
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
	        
	        foreach($products as $product_arr) {
				$product=$product_arr;
				foreach ($product as $artikal ) {
					$artikli = $artikal['attributes'];
					

	        	
	        	
	        	$sifra = $artikli['sifra'];
	        	$naziv = $artikli['naziv'];
	        	$kolicina = $artikli['kolicina'];
	        	$grupa = $artikli['vrsta'];
	        	$cena_nc = $artikli['cenarabat'];


				if(isset($sifra) && isset($cena_nc) && $sifra!=null && $cena_nc!=null){

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv) . "',";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($grupa) . "',";
					
					$sPolja .= " pdv,";						$sVrednosti .= " " . number_format(20.00,2,'.','') . ",";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			self::autoDownload();
			$products_file = "files/printcom/printcom.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;	
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
	        
			
 			foreach($products as $product_arr) {
				$product=$product_arr;
				foreach ($product as $artikal ) {
				
				$artikli = $artikal['attributes'];
	        	$sifra = $artikli['sifra'];
	        	$naziv = $artikli['naziv'];
	        	$kolicina = $artikli['kolicina'];
	        	$grupa = $artikli['vrsta'];
	        	$cena_nc = $artikli['cenarabat'];


				if(isset($sifra) && isset($cena_nc) && $sifra!=null && $cena_nc!=null){

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				}
			}	
			Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public function autoDownload(){
    
    $username="ljubo";
    $password="ljubo135";
    $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://portal.wings.rs/api/v1/printcom/system.user.log?output=jsonapi",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => '{"aUn": "'.$username.'","aUp": "'.$password.'"}',
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          $error = "cURL Error #:" . $err;
          return false;
        } else {

          $result = json_decode($response);
          $token = $result->data[0]->attributes->token;
        }    
      
        $magacin_id = 0;

        $url = "https://portal.wings.rs/api/v1/printcom/local.cache.artikal?dStart=0&dLength=10000&output=jsonapi";

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_TIMEOUT => 300000,
          CURLOPT_HTTPHEADER => array(
            "Cookie: PHPSESSID=".$token.""
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          $error = "cURL Error #:" . $err;
          return false;
        } else {
          $file = fopen('files/printcom/printcom.json','w');
          fwrite($file, $response);
          fclose($file);
        }        
	}
}