<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;

class Asbis {

	public static function execute($dobavljac_id,$kurs=null){
		
		Support::initIniSetup();
		Support::initQueryExecute();
		if($kurs==null){
			$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
		}

		$links = explode('---',Support::autoLink($dobavljac_id));
		Support::autoDownload($links[0],'files/asbis/asbis_xml/asbis.xml');
		$products = simplexml_load_file("files/asbis/asbis_xml/asbis.xml");


		$products_2 = $products->xpath('Product');

		//Ubacujem artikle u dobavljac_cenovnik_temp
		foreach ($products_2 as $product):

			$opis = $product->xpath('AttrList/element');
	    	$flag_opis_postoji = "0";
			foreach($opis as $opis_line){
				DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
				."VALUES(".$dobavljac_id."," . Support::quotedStr($product->ProductCode) . ",".Support::quotedStr(Support::encodeTo1250($opis_line->attributes()->Name)).","
				." ".Support::quotedStr(Support::encodeTo1250($opis_line->attributes()->Value)).") ");
				$flag_opis_postoji = "1";
			}

			$images = $product->xpath('Images/Image');
			$flag_slika_postoji = "0";
			$i = 0;
			foreach ($images as $slika):
				if($i==0){
					$sqlSlikaInsert = "INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->ProductCode).",'".$slika."',1 )";
				}else{
					$sqlSlikaInsert = "INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->ProductCode).",'".$slika."',0 )";
				}
				DB::statement($sqlSlikaInsert);
				$i++;
				$flag_slika_postoji = "1";
			endforeach;	

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->ProductCode) . ",";
			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr($product->Vendor) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeToIso($product->ProductCategory)) . ",";
			$sPolja .= "podgrupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeToIso($product->ProductType)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeToIso($product->ProductDescription) . " ( " .$product->ProductCode. " ) ") . ",";
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";
			$sPolja .= "pdv,";						$sVrednosti .= "" . 20 . ",";
			$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $flag_slika_postoji . "";
					
		
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
			
		endforeach;		
			
	//Ako ide fajl sa kolicinom i cenama		 
		//Citamo vrstu cene za nabavnu cenu
		$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
	
		Support::autoDownload($links[1],'files/asbis/asbis_xml/asbis2.xml');
	
		//Citam XML fajl

		$products = simplexml_load_file("files/asbis/asbis_xml/asbis2.xml");
		$products_2 = $products->xpath('PRICES/PRICE');
		
		$result_arr = array();
		$id = 0;
		foreach ($products_2 as $product):

			$result_arr[] = "(".$id.",".$dobavljac_id.",".Support::quotedStr($product->WIC).",-1,0,NULL,20,-1,NULL,NULL,-1,NULL,-1,".($product->AVAIL != 'po zahtevu' ? "1" : "0").",0,".Support::replace_empty_numeric($product->MY_PRICE,1,$kurs,$valuta_id_nc).",0,0,0,0,0,0,0,0,0,1,1,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL,0,0,0,NULL,0,NULL)";

		endforeach;
	
		self::query_update(implode(',',$result_arr),array('kolicina','cena_nc'));	
		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
	}

	public static function executeShort($dobavljac_id,$kurs=null){
		
		Support::initQueryExecute();
		if($kurs==null){
			$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
		}

		$links = explode('---',Support::autoLink($dobavljac_id));
		 
		//Citamo vrstu cene za nabavnu cenu
		$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
	
		Support::autoDownload($links[1],'files/asbis/asbis_xml/asbis2.xml');
	
		//Citam XML fajl

		$products = simplexml_load_file("files/asbis/asbis_xml/asbis2.xml");
		$products_2 = $products->xpath('PRICES/PRICE');
		
	
		foreach ($products_2 as $product):
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->WIC) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->AVAIL != 'po zahtevu' ? "1," : "0,";

			$sPolja .= "cena_nc";					$sVrednosti .= "" . Support::replace_empty_numeric($product->MY_PRICE,1,$kurs,$valuta_id_nc) . "";

			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
			
		endforeach;
	
		//Support::queryShortExecute($dobavljac_id, $type);
	}

	public static function query_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik_temp'"));
		$table_temp = "(VALUES ".$table_temp_body.") dobavljac_cenovnik_temp_temp(".implode(',',$columns).")";

		//update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		foreach($columns as $col){
			if($col!="dobavljac_cenovnik_id" && $col!="sifra_kod_dobavljaca"){
		    	$updated_columns[] = "".$col." = dobavljac_cenovnik_temp_temp.".$col."";
			}
		}
		DB::statement("UPDATE dobavljac_cenovnik_temp t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_kod_dobavljaca=dobavljac_cenovnik_temp_temp.sifra_kod_dobavljaca AND t.partner_id=dobavljac_cenovnik_temp_temp.partner_id");
	}
	
}