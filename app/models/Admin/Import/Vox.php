<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Vox {

	public static function ftpConnection() {
		$ftp_host = '94.127.7.141';
		$ftp_user_name = 'tempokg@black666.mycpanel.rs';
		$ftp_user_pass = 'tempokg2019123';

            $connection = ftp_connect($ftp_host);
            $login_result = ftp_login($connection,$ftp_user_name,$ftp_user_pass);
            ftp_pasv($connection, true);

            if(ftp_size($connection, "erg.xml") != -1){
			ftp_get($connection,'files/vox/vox_xml/vox.xml','erg.xml', FTP_BINARY);
           
            }

            ftp_close($connection);
	}
	
	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		
		$extension = 'xml';
		if($extension==null){
			$products_file = "files/vox/vox_xml/vox.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			self::ftpConnection();
			$continue = true;
			$products_file = 'files/vox/vox_xml/vox.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	
			foreach ($products as $product):
				if(!empty($product->id)){

					$slika = 'http://www.voxelectronics.com/files/images/slike_proizvoda/'.trim($product->id).'.jpg' ;
				
					$sPolja = '';
					$sVrednosti = '';
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->id))) . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" .'VOX ' . addslashes(Support::encodeTo1250($product->name) . " ( " . Support::encodeTo1250(trim($product->id)) . " )") . "',";
					$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->category)) . "',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . str_replace('>','',$product->stock) . ",";
					$sPolja .= "flag_slika_postoji,";		$sVrednosti .= "1,";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '', $product->price),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr(trim($product->id)).",'".Support::encodeTo1250($slika)."',1 )");
				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		$extension = 'xml';
		if($extension==null){
			$products_file = "files/vox/vox_xml/vox.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			self::ftpConnection();
			$continue = true;
			$products_file = 'files/vox/vox_xml/vox.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	
   
			foreach ($products as $product):
				if(!empty($product->id)){

					$slika = 'http://www.voxelectronics.com/files/images/slike_proizvoda/'.trim($product->id).'.jpg' ;
				
					$sPolja = '';
					$sVrednosti = '';
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . str_replace('>','',$product->stock) . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '', $product->price),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}