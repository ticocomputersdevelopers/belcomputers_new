<?php

class AdminStraniceController extends Controller {

    public function page_section($page_section_id,$page_section_type_id=null,$lang_id=null){
    	if(is_null($page_section_type_id)){
    		if(!is_null($page_section_id) && $page_section_id > 0){
    			$page_section_type_id = DB::table('sekcija_stranice')->where('sekcija_stranice_id',$page_section_id)->pluck('sekcija_stranice_tip_id');
    		}else{
    			$page_section_type_id = DB::table('sekcija_stranice_tip')->orderBy('rbr','asc')->pluck('sekcija_stranice_tip_id');
    		}
    	}
    	if(is_null($lang_id)){
    		$lang_id = AdminLanguage::defaultLangObj()->jezik_id;
    	}

    	$data = array(
    		'title' => 'Sekcija stranice',
    		'strana' => 'sekcija_stranice',
    		'sections' => DB::table('sekcija_stranice')->orderBy('sekcija_stranice_id','asc')->get(),
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
    		'page_section' => $page_section_id > 0 ? DB::table('sekcija_stranice')->where('sekcija_stranice_id',$page_section_id)->first() : (object) ['sekcija_stranice_id' => 0, 'sekcija_stranice_tip_id' => $page_section_type_id, 'naziv'=>'', 'flag_aktivan' => 1, 'slajder_id' => 0, 'tip_artikla_id' => 0],
    		'page_section_lang' => $page_section_id > 0 && !is_null($page_section_lang=AdminStranice::sectionLang($page_section_id,$lang_id)) ? $page_section_lang : (object) ['sadrzaj' => ''],
    		'page_section_type' => AdminStranice::getSectionType($page_section_type_id),
    		'lang_id' => $lang_id
    	);
        return View::make('admin/page',$data); 
    }

    public function page_section_edit(){
    	$data = Input::get();
    	$sekcija_stranice_id = $data['sekcija_stranice_id'];
    	$jezik_id = $data['jezik_id'];
    	unset($data['sekcija_stranice_id']);
    	unset($data['jezik_id']);
        $tipNaziv = DB::table('sekcija_stranice_tip')->where('sekcija_stranice_tip_id',$data['sekcija_stranice_tip_id'])->pluck('naziv');

        $validator = Validator::make($data,
        array(
            'sekcija_stranice_tip_id' => 'required|integer',
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:255',
            'flag_aktivan' => 'required|in:0,1',
            'slajder_id' => ($tipNaziv == 'Slajder' ? 'required|' : '').'integer',
            'tip_artikla_id' => ($tipNaziv == 'Lista artikala' ? 'required|' : '').'integer'
        ),
        array(
    		'required' => 'Niste popunili polje!',
    		'regex' => 'Unesite odgovarajuči karakter!',
    		'max' => 'Uneli ste neodgovarajući broj karaktera!',
    		'integer' => 'Vrednost mora da bude broj',
    		'in' => 'Unesite odgovarajuči karakter!'
        ));

        if($validator->fails()){
            // return Redirect::back()->withInput()->withErrors($validator->messages());
            return Redirect::to(URL::previous() . "#page_id")->withInput()->withErrors($validator->messages());
        }else{
        	if($sekcija_stranice_id == 0){
        		DB::table('sekcija_stranice')->insert([
		            'sekcija_stranice_tip_id' => $data['sekcija_stranice_tip_id'],
		            'naziv' => $data['naziv'],
		            'flag_aktivan' => $data['flag_aktivan'],
                    'puna_sirina' => $data['puna_sirina'],
                    'boja_pozadine' => $data['boja_pozadine'] != '#000000' ? $data['boja_pozadine'] : null,
		            'slajder_id' => isset($data['slajder_id']) && !empty($data['slajder_id']) ? $data['slajder_id'] : null,
		            'tip_artikla_id' => isset($data['tip_artikla_id']) && !empty($data['tip_artikla_id']) ? $data['tip_artikla_id'] : null,
        		]);

        		$sekcija_stranice_id = DB::table('sekcija_stranice')->max('sekcija_stranice_id');
        	}else{
        		DB::table('sekcija_stranice')->where('sekcija_stranice_id',$sekcija_stranice_id)->update([
		            'sekcija_stranice_tip_id' => $data['sekcija_stranice_tip_id'],
		            'naziv' => $data['naziv'],
		            'flag_aktivan' => $data['flag_aktivan'],
                    'puna_sirina' => $data['puna_sirina'],
                    'boja_pozadine' => $data['boja_pozadine'] != '#000000' ? $data['boja_pozadine'] : null,
		            'slajder_id' => isset($data['slajder_id']) ? $data['slajder_id'] : null,
		            'tip_artikla_id' => isset($data['tip_artikla_id']) ? $data['tip_artikla_id'] : null,
        		]);
        	}

        	if(isset($data['sadrzaj']) && !empty($data['sadrzaj'])){ 		
	    		if(!is_null($sekcija_stranice_jezik = DB::table('sekcija_stranice_jezik')->where(['sekcija_stranice_id'=>$sekcija_stranice_id,'jezik_id'=>$jezik_id])->first())){

	        		DB::table('sekcija_stranice_jezik')->where('sekcija_stranice_jezik_id',$sekcija_stranice_jezik->sekcija_stranice_jezik_id)->update([
			            'sadrzaj' => $data['sadrzaj']
	        		]);
	    		}else{
	        		DB::table('sekcija_stranice_jezik')->insert([
			            'sekcija_stranice_id' => $sekcija_stranice_id,
			            'jezik_id' => $jezik_id,
			            'sadrzaj' => $data['sadrzaj']
	        		]);
	    		}
        	}

        	// return Redirect::to('/admin/page-section/'.$sekcija_stranice_id.'/'.$data['sekcija_stranice_tip_id'].'/'.$jezik_id)->with('message','Uspešno ste sačuvali podatke.');
            return Redirect::to(URL::previous() . "#page_id")->with('message','Uspešno ste sačuvali podatke.');

        }    
    }

    public function page_section_delete($page_section_id){
    	DB::table('sekcija_stranice')->where('sekcija_stranice_id',$page_section_id)->delete();
    	// return Redirect::to('/admin/page-section/0');
        return Redirect::to(URL::previous() . "#page_id")->with('message','Uspešno ste sačuvali podatke.');
    }

}