$(document).ready(function() {
	$('#JSPartnerVrstaFilter').change(function(){
		var params = getUrlVars();
		params['partner_vrsta_id'] = $(this).val();
		delete params['page'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSPartnerApiFilter').change(function(){
		var params = getUrlVars();
		params['api_aktivan'] = $(this).val();
		delete params['page'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSPartnerB2BAccessFilter').change(function(){
		var params = getUrlVars();
		params['b2b_access'] = $(this).val();
		delete params['page'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	var partner_id = $('#JSCheckPartnerConfirm').data("id");
	var dokumenti = $('#JSCheckPartnerConfirm').data("dokumenti")=='1';
	if(partner_id!=0 && partner_id!=null){
		var check = confirm(translate("Partner je vezan za narudžbine, import ili artikle! Da li želite obrisati sve sto je vezano za ovog partnera?"));
		if(check){
			location.href = base_url+(dokumenti ? 'dokumenti/partner/' : 'admin/kupci_partneri/partneri/')+partner_id+'/1/delete';
		}
	}
	$('.JSImport').on('click', function(){
	$('#JSchooseImport').foundation('reveal', 'open');
	});

	$('.JSImportPartneri').click(function(){
		var names = $('.JSKolonaImport:checked').map(function(idx, elem) {
		    return $(elem).data('name');
		  }).get();
		var target_url = $(this).data('link');
		var target_url_arr = target_url.split('/');

		var url_names = 'null';
		if(names.length > 0){
			url_names = names.join('-');
		}
		target_url_arr[7] = url_names;
		
		location.href= target_url_arr.join('/');

	});

	$('.JSExport').on('click', function(){
	$('#JSchoose').foundation('reveal', 'open');
	});

	$('.JSExportPartneri').click(function(){
		var names = $('.JSKolonaExport:checked').map(function(idx, elem) {
		    return $(elem).data('name');
		  }).get();

		var url_names = 'null';
		if(names.length > 0){
			url_names = names.join('-');
		}

		var params = getUrlVars();
		params['export'] = 1;
		params['vrsta_fajla'] = $(this).data('kind');
		params['imena_kolona'] = url_names;

		window.location.href = window.location.pathname+'?'+$.param(params);
	});

});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value.replace('#','');
    });
    return vars;
}