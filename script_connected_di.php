<?php
require_once("modules/classes/all.php");
if(isset($_POST['action'])){ $action = $_POST['action']; } else { $action = "-1"; } // Akcija koja se primenjuje
if(isset($_POST['selektovani'])){ $selektovani = $_POST['selektovani']; } else { $selektovani = "-1"; } // Akcija koja se primenjuje
//U zavisnosti od akcije
// $action = "0";
// $selektovani = 349904;
switch($action){
	case "0":
		$misc = new Misc($pgConn);
		//Prvo vracamo roba_id za izabrane artikle
		$sql = "SELECT dobavljac_cenovnik_id,sifra_kod_dobavljaca,partner_id,roba_id,flag_opis_postoji,flag_slika_postoji FROM dobavljac_cenovnik "
  		."WHERE dobavljac_cenovnik_id IN(".$selektovani.") AND roba_id <> -1 AND povezan = 1";
		$pgConn->query($sql,"connected_di",array());
		$roba = array();
        while($row = $pgConn->fetchArray()){
            $roba[]=$row;
        };


        $rezultat = '-1';
        //Prvo cemo za svaki artikal da proverimo ako ima opis
        foreach ($roba as $artikal) {
        	if($artikal['flag_opis_postoji']=='1'){
        		//Uradi update u tabeli dobavljac_cenovnik_karakteristike za roba_id
        		$sql = "UPDATE dobavljac_cenovnik_karakteristike SET roba_id = $1 WHERE partner_id = $2 AND sifra_kod_dobavljaca = $3";
        		$pgConn->query($sql,"connected_di_update".$artikal['dobavljac_cenovnik_id'],array($artikal['roba_id'],$artikal['partner_id'],$artikal['sifra_kod_dobavljaca']));

        		//Uradi update u tabeli roba da je flag za karaketistike od dobavljaca ako je prosao update
        		if($pgConn->affectedRows()>0){
        			$sql = "UPDATE roba SET web_flag_karakteristike = 2 WHERE roba_id = $1";
        			$pgConn->query($sql,"connected_di_update_roba".$artikal['roba_id'],array($artikal['roba_id']));
        			
        		}
        		$rezultat = '0';	
        	}else{
        		$rezultat = '-1';
        	}	

        	//Prvo cemo za svaki artikal da uzmemo sve slike koje trenutno ima i da njihov web_slika_id smestimo u array
        	$sql = "SELECT web_slika_id FROM web_slika WHERE roba_id = $1";
        	$pgConn->query($sql,"connected_di_slike_postojece",array($artikal['roba_id']));
			$postojece_slike = array();
	        while($row = $pgConn->fetchArray()){
	            $postojece_slike[]=$row;
	        };

	        //Onda cemo da uzmemo slike iz dobavljac_cenovnik_slike za datog partnera i sifru kod dobavljaca
	        $sql = "SELECT akcija,putanja FROM dobavljac_cenovnik_slike WHERE partner_id = $1 AND sifra_kod_dobavljaca = $2";
	        $pgConn->query($sql,"connected_di_slike_dobavljac",array($artikal['partner_id'],$artikal['sifra_kod_dobavljaca']));
			$slike = array();
	        while($row = $pgConn->fetchArray()){
	            $slike[]=$row;
	        };


	        $skinute_slike_postoje = false;
	        //Za svaku sliku od dobavljaca, skidamo je i ubacujemo kao novu za dati artikal
	        foreach ($slike as $slika_dobavljac) {
	        	$web_slika_id = $misc->miscGetSequence('web_slika_web_slika_id_seq');

				//Extraktujem ekstenziju
				$path_parts = pathinfo($slika_dobavljac['putanja']);
			
				//Formiram naziv fajla
				$putanja = 'images/products/big/'.$web_slika_id.'.'.$path_parts['extension'];
			
				//Skidam sliku i smestam je u folder standard
				$content = file_get_contents($slika_dobavljac['putanja']);
				$fp = fopen("images/products/big/".$web_slika_id.'.'.$path_parts['extension'], "w"); 
				fwrite($fp, $content); 
				fclose($fp);

				//Ako je skinuta vrsim upis u bazu
				if(file_exists("images/products/big/".$web_slika_id.'.'.$path_parts['extension'])){
					$sql = "INSERT INTO web_slika(roba_id,akcija,putanja)VALUES(".$artikal['roba_id'].",".$slika_dobavljac['akcija'].",'".$putanja."')";
					//echo $sql."<br />";
					$pgConn->query($sql,"connected_di_slike_unesi".$web_slika_id,array());
					$skinute_slike_postoje = true;
				}
	        }
	        //Ako su slike skinute, necemo brisati stare nego cemo samo da ih skinemo sa prikazivanja
	        if($skinute_slike_postoje==true){
		        foreach ($postojece_slike as $slike_exist) {
		        	$sql = "UPDATE web_slika SET flag_prikazi = 0,akcija=0 WHERE web_slika_id = $1";
		        	$pgConn->query($sql,"connected_di_slike_promeni_prikaz_2".$slike_exist['web_slika_id'],array($slike_exist['web_slika_id']));
		        }
	        }	
	        
        }
        echo $rezultat;
	break;
}
?>